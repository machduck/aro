*Aro, you are my fuzzer..*

**Feedback is really appreciated!**

*Although, this code is a pile of POC..*

What is Aro?
============
Aro is an **interactive fuzzer**.
It's primary function is to script fuzzing from a python console.
Preferably one that supports autocompletion and syntax highlighting: think bpython or ipython.
The workflow is as follows:

* open up a python console
* import aro
* import or create queries
* script your fuzzing
* ???
* PROFIT!!!

Why should i use an interactive fuzzer?
=======================================
* Testing is **explicit** - you can't just push a button, you must write some code.
  This creates understanding of what it is that you've just tested and have not yet tested.
* Tight **scope control** - suppose some parts of the resource are out of scope.
  No problem! Exclude the unwanted queries at fuzz time or fuzz only the ones you want!
* Tight **vulnerability control** - let's say you only want to test for one or two vulnerability types or only POST requests.
  Aro's got your back! The API was specifically designed with this in mind.
* Instead of manually trying a couple types of injections in the parameter and missing a blind injection vulnerability
  due to correct discrete error handling, fuzzing the request parameter will increase the chances of finding said vulnerability
  while retaining the control of manual testing.

But i'm not a fan of interactive fuzzing..
==========================================
There's nothing stopping you from writing scripts.

Help me better understand this concept, is there some analogy?
==============================================================
Semi-automatic guns. A crowded environment warrants the use of surgical tools instead of spray and pray counterparts.
This is where Aro shines. Execute testing with surgical percision to avoid collateral damage.

Perks!
======
* multithreaded **connection pool with keep-alive** reducing fuzz time
* JSON and wordlist formats for fuzzing and anomoly detection
* **fuzzy anomoly analysis**
* frequency and position dependant anomoly analysis
* white list and black list technology filtering
* real time **adaptive technology filtering**
* **camouflage** via transformations of requests
* random order of requests
* random request wait times
* parameter level **scope specification**
* based on community regarded fuzz lists such as
	* FuzzDB
	* Seclist
* custom anomoly and non-anomoly callbacks for result handling
* request **retransmission on network failures**
* easy integration into existing SDLC and regression testing
* **csrf token fetching** and hooks
* authenticated csrf token fetching support
* recursive token resolution
* authentication hooks
* **automatic reauthentication** with session juggling
* configurable multithreaded request **body prefetching** for optimal anomoly analysis
* order-specific request chaining
* constant level of worker threads allowing for **optimal channel load**
* multithreaded anomoly **reflection checks** via reusing freed threads
* can be used as a **manual testing tool with anomoly detection**

All of this comes together to create an extensible and modifiable web fuzzing framework
with just the right level of abstraction to reduce bloat.

The dark side..
===============
* not packaged
* no comments
* no documentation
* 0% test coverage (scrap current tests)
	* constants are NOT imported from code tree
* random iterator absurdly unoptimized (do via LCG)
* architecture is stretched to the limit - scrap the engine
* python3 not yet supported
* optimize prefetching
	* via memoization of responses to reduce total amount of requests (do via weakref.WeakKeyDictionary)
		* prefetching
			* fuzzing
			* manual testing
			* reflections
	* via peekahead queueing to utilize all threads
		* clonable iterator
* other stuff to do:
	* stealth rating
		* request wait times
		* user activity emulation with legit requests
	* obfuscation rating for FuzzQuery transformations
	* common transformations
	* common technology detects
	* common payload grouping between fuzzers
	* fuzzers for common vulnerabilities
	* request chain hooks
	* disable url encoding for specific sequence elements

Example:
========

```pycon
>>> q1 = Query( url = 'php5fpm.nginx/sqli2.php?one=1&two=2&three=3&four=4' )
>>> q2 = Query( url = 'php5fpm.nginx/sqli.php?one=1&two=2&three=3&four=4' )
>>> fuzzer.fuzz({ q1: GETFuzzer( Fuzz.SQL ), q2: GETFuzzer( Fuzz.SQL ) }).join()
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli2.php GET four=4">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli2.php GET four=4'>
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli2.php GET four=4\">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli2.php GET four=4\'>
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli2.php GET three=3\">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli2.php GET three=3">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli2.php GET three=3\'>
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli2.php GET three=3'>
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli2.php GET two=2'>
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli2.php GET two=2">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli2.php GET two=2\">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli2.php GET two=2\'>
[01/26/2014 01:59:56 WARNING] [!] ANOMOLY <Query @http://php5fpm.nginx/sqli2.php GET one=1">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli2.php GET one=1\">
[01/26/2014 01:59:56 WARNING] [!] ANOMOLY <Query @http://php5fpm.nginx/sqli2.php GET one=1'>
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli2.php GET one=1\'>
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET four=4">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET four=4'>
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET four=4\">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET four=4\'>
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET three=3">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET three=3'>
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET three=3\">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET three=3\'>
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET two=2">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET two=2\">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET two=2'>
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET two=2\'>
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET one=1'>
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET one=1\">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET one=1">
[01/26/2014 01:59:56 INFO] [*] OK <Query @http://php5fpm.nginx/sqli.php GET one=1\'>
<fuzzer.Fuzzer object at 0x3038490>
>>> loot()
[*] +OK 2
[*] <Query @http://php5fpm.nginx/sqli2.php GET one=1">
[!] <Detected SQLi @sqli [u'sqli']>
[*] <Query @http://php5fpm.nginx/sqli2.php GET one=1'>
[!] <Detected SQLi @sqli [u'sqli']>
{<Query @http://php5fpm.nginx/sqli2.php GET one=1">: [<Detected SQLi @sqli [u'sqli']>],
 <Query @http://php5fpm.nginx/sqli2.php GET one=1'>: [<Detected SQLi @sqli [u'sqli']>]}
>>>
```

Demo
====

TODO: embed from ascii.io

Quick Start
===========

Context:

```py
f = FuzzerFactory()
f = Fuzzer()
f = FuzzerFactory().create()
```

FuzzerFactory creates Fuzzer via passing on its internal state to the fuzzer.

Check out

* /src/bootstrap.py
* /src/skel.py
* /tests/test_fuzzer.py

for framework initialisation scenerios.

Check out /tests/test_fuzzer.py to view examples of

* creating queries
* fuzzing queries
* using fuzzgroups
* fuzzing multiple queries in one go
* scope inclusion and exclusion
* xsrf token resolution
* authentication
* reauthentication
* deauthentication
* using reflections

Queries
-------

Query

```py
q = Query( url = 'site.com/path?one=1&two=2&three=3' )
```
Has a LOT of settings, check out:

* /src/query.py
* /src/request.py

Query with XSRF token (will fetch token automatically and inject instead of the *[@TOKEN]* parameter value in ANY field type)

```py
q = Query( url = 'site.com/path?one=1&two=2&three=3&token=[@TOKEN]',
           token = Token( url = 'site.com/token,
                          extract = 'X-XSRF',
                          type = 'header' ) )
```

**type**:
One of the following:

* 'header'
* 'cookie'
* 'xpath'
* 'regex'

**extract**:
The key by which to find the token.

Query with authentication (will auth automatically via FuzzerAuth)

```py
q = Query( url = 'site.com/path?one=1&two=2&three=3',
           doauth = True )
```

Query explicitly without authentication (will deauth automatically via FuzzerAuth)

```py
q = Query( url = 'site.com/path?one=1&two=2&three=3',
           doauth = False )
```

Set *doauth* to None if either priv is ok.

Query with reflections (will be fetched automatically and checked for anomolies in the context of the original query)

```py
q = Query( url = 'site.com/path?one=1&two=2&three=3',
           reflections = [
               Query( url = 'site.com/reflection1' ),
               Query( url = 'site.com/reflection2' ),
           ])
```

Query with order-specific request chaining (will be fetched automatically in the specified order)

```py
q = Query( url = 'site.com/path?one=1&two=2&three=3',
           chain = [
               Query( url = 'site.com/link1' ),
               Query( url = 'site.com/link2' ),
           ])
```

**Note!**
Reflections and XSRF tokens can have their own XSRF tokens and authentication options.

**Note!**
Reflections and XSRF tokens support hooks.

Field Generators
----------------

Create field generator:

```py
GETFuzzer( Fuzz.SQL | Fuzz.XSS )
```

Accepts a bitmask of fuzzers or defaults to Fuzz.ALL

Fuzzing initiated by issuing:

```
f.fuzz({ [QUERY]: [FIELD_GENERATOR] })
f.fuzz({ [QUERY]: FuzzGroup( [FIELD_GENERATOR1], [FIELD_GENERATOR2], .. ) })
```

**GETFuzzer**:
Get parameters. Ex: ?one=1

** POSTFuzzer**:
Post parameters.

**PathFuzzer**:
Path values. Ex: /[path]/[path]

**CookieFuzzer**:
Cookies.

**HeaderFuzzer**:
Headers.

**FileFuzzer**:
Files.

**GenericFuzzer**:
All of the above.

Consider using the following constructions:

```
q1 = Query(..)
q2 = Query(..)
q3 = Query(..)
f.fuzz(dict( zip([ q1,q2,q3 ], [GETFuzzer( Fuzz.SQL | Fuzz.XSS )]*3 ))).join()

qa = [
	Query(..),
	Query(..),
	Query(..),
]
f.fuzz(dict( zip(qa, [GETFuzzer( Fuzz.SQL | Fuzz.XSS )]*len(qa) ))).join()
```

Saving and loading
------------------

```pycon
>>> q = Query()
>>> q.save( '~/q' )
[01/26/2014 01:34:07 INFO] [+] saved <Query @None> to ~/q
>>> del q
>>> q = load( '~/q' )
[01/26/2014 01:34:19 INFO] [+] loaded <Query @None> from /home/folder/q
```

Anomoly Monitoring
------------------

```py
f.anomoly = FuzzerAnomoly( Anomoly.DIFF | Anomoly.CODE | Anomoly.TIME | Anomoly.HASH | Anomoly.X_HEADER | Anomoly.CONTEXT )
```

**DIFF**:
Fuzzy matching using query-specific ratios to detect major changes in pages. Best works with prefetching.

**CODE**:
Detects interesting http codes ( everything BUT FuzzerAnomoly.X_CODES, reference FuzzerAnomoly.X_CODES_EXTRA ).

**TIME**:
Detects lengthy time delays using query-specific statistics.

**HASH**:
Detects 16,32,64 bit hashes (best works with prefetching).

**X_HEADER**:
Detects interesting http headers (currently empty).

**CONTEXT**:
Detects fuzzer-specific anomolies.

The above is equal to:

```py
f.anomoly = FuzzerAnomoly( Anomoly.ALL )
```

Detects are filtered via:

```py
f.anomoly = FuzzerAnomoly( Anomoly.ALL | Anomoly.DEFAULT )
f.anomoly = FuzzerAnomoly( Anomoly.ALL | Anomoly.FREQUENCY )
f.anomoly = FuzzerAnomoly( Anomoly.ALL | Anomoly.SMART )
```

**DEFAULT**:
Yields ANY anomolies.

**FREQUENCY**:
Yields all anomolies which differ in frequency analysis between fuzz response and prefetched responses.

**SMART**:
Yields all anomolies which differ in position between fuzz response and prefetched responses.

```py
f.anomoly = FuzzerAnomoly( Anomoly.ALL | Anomoly.FREQUENCY | Anomoly.FILTER )
f.anomoly = FuzzerAnomoly( Anomoly.ALL | Anomoly.SMART | Anomoly.FILTER )
```
**FILTER**:
Employ adaptive fuzzing filters based on detected technology in use by target.

Fuzzing Order
-------------

```py
f.dispatcher = FuzzerDispatcher( Order.RANDOM )
f.dispatcher = FuzzerDispatcher( order = Order.QUERY|Order.FUZZ_STRING )
f.dispatcher = FuzzerDispatcher( order = Order.QUERY|Order.PARAMETER )
f.dispatcher = FuzzerDispatcher( order = Order.FUZZER|Order.FUZZ_STRING )
f.dispatcher = FuzzerDispatcher( order = Order.FUZZER|Order.PARAMETER )
```

**RANDOM**:
Randomly select queries

Outer loop:

**QUERY**:
Outer loop is query instead of fuzzer.

**FUZZER**:
Outer loop is fuzzer instead of query.

Inner loop:

**FUZZ_STRING**:
Outer loop is the fuzz string.

**PARAMETER**:
Outer loop is the parameter.

Prefetching
-----------

```py
f.dispatcher = FuzzerDispatcher( fetch = Fetch.ERROR | Fetch.NOT_FOUND | Fetch.NULL | Fetch.VALID )
```

**ERROR**:
Force an error.

**NOT_FOUND**:
Request missing page.

**NULL**:
Missing fuzzable parameter value

**VALID**:
Original query

The above is equal to:

```py
f.dispatcher = FuzzerDispatcher( fetch = Fetch.ALL )
```

Transformations
---------------

```py
f.transformer = FuzzerTransformer( Transform.GOOGLE_CHROME ) # bitmask
```

Pool Threads
------------

```py
f.pool = FuzzerPool( 3 )
```

Tech Filtering
--------------

```py
f.tech = FuzzerTech(whitelist = { 'sql': [ 'mysql', 'mssql' ] },
					blacklist = { 'sql': [ 'generic', ] })
```

Result Hooks
------------

```py
def normal( self, query ):
	logging.info( '[*] OK %s' % query )

def anomoly( self, query ):
	logging.warning( '[!] ANOMOLY %s' % query )
	self.queue( query )

def error( self, query ):
	logging.error( '[-] ERROR %s' % query )

f.result = FuzzerResult( anomoly = anomoly, normal = normal, error = error )
```

Session Juggling
----------------

One global session:

```py
f.auth = FuzzerAuth( global_session = True )
```

Sessions never time out:

```py
f.auth = FuzzerAuth( login = Query( url = 'http://site/login?user=user&pass=pass&token=[@TOKEN]',
									token = Token( 	url = 'http://site/token',
													type='header',
													extract='X-XSRF' )))
```

Sessions alive for 30 seconds:

```py
f.auth = FuzzerAuth(login = Query( 	url = 'http://site/login?user=user&pass=pass&token=[@TOKEN]',
									token = Token( 	url = 'http://site/token',
													type='header',
													extract='X-XSRF' )),
					timeout = '30s' )
```

Sessions alive for a *total* amount of 10 seconds independant of last request:

```py
f.auth = FuzzerAuth(login = Query( 	url = 'http://site/login?user=user&pass=pass&token=[@TOKEN]',
									token = Token( 	url = 'http://site/token',
													type='header',
													extract='X-XSRF' )),
					timeout = '10s',
					reset = False )
```

Sessions reused every request causing a priv switch if a queries privs differ.
Optimized to select session with same privs as request if one exists and is currently available for use.

```py
f.auth = FuzzerAuth(login = Query( 	url = 'http://site/login?user=user&pass=pass&token=[@TOKEN]',
									token = Token( 	url = 'http://site/token',
													type='header',
													extract='X-XSRF' )),
					logout = Query( url = 'http://site/logout?token=[@TOKEN]',
									token = Token( 	url = 'http://site/token',
													type = 'header',
													extract = 'X-XSRF' )),
					context = True )
```

Helper Functions
----------------

Returns a dict mapping response queries with arrays of anomolies.

```py
r = loot()
```

Returns the query at index from the loot dict.

```py
r.query( 0 )
```

Returns an array of anomolies for the query at index from the loot dict.

```py
r.anomolies( 0 )
```

Fuzzlists
---------

* SQL Fuzz
	* value_mutation
		* append
		* overwrite
		* prepend
* SQL Detect
	* tech
		* [ mysql, mssql ]
		* [ generic ]
		* []
	* filter
		* blacklist
		* whitelist
		* none
