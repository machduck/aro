#!/usr/bin/env python

import abc

from patterns import Action, Type, Strategy, Emitter, Bitmask
from ifuzzer import iFuzzerComponent

import difflib
import re

import logging

class Analysis( list ):

	def __eq__( self, other ):
		if( other == True ):
			return True
		else:
			return list.__eq__( self, other )

	def __ne__( self, other ):
		return not self.__eq__( other )

	def __nonzero__( self ):
		if( len( self.anomolies ) > 0 ):
			return True
		else:
			return False
	
	def __bool__( self ):
		return self.__nonzero__()

class Anomoly( object ):
	NONE = 0
	DUMMY = 0
	ALL = 0b111111

	DIFF = 0b1
	CODE = 1 << 1
	TIME = 1 << 2
	X_HEADER = 1 << 3
	HASH = 1 << 4
	CONTEXT = 1 << 5

	DEFAULT =  0b000000000
	SMART = 1 << 6 | 1 << 7
	FREQUENCY = 1 << 6 | 1 << 8

	FILTER = 1 << 9

	DEMO = 1 << 0xA
	SQL = 1 << 0xB
	XSS = 1 << 0xC

class Detected( object ):
	detected = Emitter()
	detected.default( lambda: '?' )
	detected.on( Anomoly.SQL, lambda: 'SQLi' )
	detected.on( Anomoly.CODE, lambda: 'Code' )
	detected.on( Anomoly.DIFF, lambda: 'Diff' )
	detected.on( Anomoly.HASH, lambda: 'Hash' )
	detected.on( Anomoly.TIME, lambda: 'Time' )
	detected.on( Anomoly.X_HEADER, lambda: 'Header' )
	detected.on( Anomoly.DEMO, lambda: 'Demo' )

	def __init__( self, anomoly, trigger = None, meta = None ):
		self.anomoly = anomoly
		self.name = self.detected.emit( anomoly )
		self.trigger = trigger
		self.meta = meta

	def __repr__( self ):
		if( self.meta is not None ):
			return '<Detected {} @{} {}>'.format( 	self.name,
													self.trigger,
													self.meta )
		else:
			return '<Detected %s @%s>' % ( self.name, self.trigger )

class Reflected( object ):
	def __init__( self, query, detected ):
		self.query = query
		self.anomoly = detected.anomoly
		self.name = detected.name
		self.trigger = detected.trigger
		self.meta = detected.meta
	
	def __repr__( self ):
		return '<Reflected %s @%s %s>' % ( 	self.name,
											self.trigger, 
											self.query )

def diff( self, response ):
	if( response.ignored() ):
		return

	ratio = lambda old: difflib.SequenceMatcher(None,
												old,
												response.response.body).ratio()
	triggered = False
	active = False

	body_ratio = 0.0
	null_ratio = 0.0
	error_ratio = 0.0
	not_found_ratio = 0.0
	index_ratio = 0.0
	rel_index_ratio = 0.0
	rel_not_found_ratio = 0.0

	if( response.request.body is not None ):
		active = True
		body_ratio = ratio( response.request.body )
		if(  body_ratio > response.ratio and not triggered ):
			triggered = True
	if( response.request.null is not None ):
		active = True
		null_ratio = ratio( response.request.null )
		if( null_ratio > response.ratio and not triggered ):
			triggered = True
	if( response.request.error is not None ):
		active = True
		error_ratio = ratio( response.request.error )
		if( error_ratio > response.ratio and not triggered ):
			triggered = True
	if( response.request.not_found is not None ):
		active = True
		not_found_ratio = ratio( response.request.not_found )
		if( not_found_ratio > response.ratio and not triggered ):
			trigerred = True
	if( response.request.index is not None ):
		active = True
		index_ratio = ratio( response.request.index )
		if( index_ratio > response.ratio and not triggered ):
			trigerred = True
	if( response.request.rel_index is not None ):
		active = True
		rel_index_ratio = ratio( response.request.rel_index )
		if( rel_index_ratio > response.ratio and not triggered ):
			trigerred = True
	if( response.request.rel_not_found is not None ):
		active = True
		rel_not_found_ratio = ratio( response.request.rel_not_found )
		if( rel_not_found_ratio > response.ratio and not triggered ):
			trigerred = True
	logging.debug( '[RATIO] ' \
			+ 'body: {:.3f}, null: {:.3f}, error: {:.3f}, not_found: {:.3f} ' \
			+ 'index: {:.3f}, rel_index: {:.3f}, rel_not_found: {:.3f}' \
			.format(body_ratio, 
					null_ratio,
					error_ratio,
					not_found_ratio, 
					rel_not_found_ratio))
	if( not triggered and active ):
		response.anomoly( 	Anomoly.DIFF,
							trigger = '{}' \
									.format( response.ratio ))

def code( self, query ):
	if( not query.ignored() and query.response.code not in self.x_codes ):
		query.anomoly( Anomoly.CODE, trigger = query.response.code )

def time( self, query ):
	if( self.x_time is None ):
		return
	if( query.response.rtt > self.x_time ):
		query.anomoly( 	Anomoly.TIME,
						trigger = "{:.2f}".format( query.response.rtt ))

def x_header( self, query ):
	if( query.ignored() or len(self.x_headers.keys()) == 0 ):
		return
	for x_header, x_value in self.x_headers.iteritems():
		for q_header, q_value in query.response.headers.iteritems():
			if( x_header == q_header ):
				if( x_value is None ):
					query.anomoly( 	Anomoly.X_HEADER,
									trigger = x_header )
				elif( x_value == q_value ):
					query.anomoly( 	Anomoly.X_HEADER,
									trigger = x_header,
									meta = x_value )

def smart_analysis( regex, query ):
	data_sets = (
		query.request.body,
		query.request.null,
		query.request.error,
		query.request.not_found,
	)
	anomolies = regex.findall( query.response.body )
	if( len( anomolies ) == 0 ):
		return False
	alert = True
	for data_set in data_sets:
		if data_set is not None:
			if( regex.findall( data_set ) == anomolies ):
				alert = False
				break
	if( alert ):
		return Analysis( anomolies )

def frequency_analysis( regex, query ):
	data_sets = (
		query.request.body,
		query.request.null,
		query.request.error,
		query.request.not_found,
	)
	anomolies = regex.findall( query.response.body )
	if( len( anomolies ) == 0 ):
		return False
	alert = True
	for data_set in data_sets:
		if data_set is not None:
			if( len( regex.findall( data_set )) == len( anomolies )):
				alert = False
				break
	if( alert ):
		return Analysis( anomolies )

def default_analysis( regex, query ):
	anomolies = regex.findall( query.response.body )
	if( len( anomolies ) > 0 ):
		return Analysis( anomolies )

analysis = Emitter()
analysis.default( default_analysis )
analysis.on( Anomoly.SMART, smart_analysis )
analysis.on( Anomoly.FREQUENCY, frequency_analysis )

hash_regexes = [
	re.compile( '[a-fA-F0-9]{16}' , re.S | re.U | re.X ),
	re.compile( '[a-fA-F0-9]{32}' , re.S | re.U | re.X ),
	re.compile( '[a-fA-F0-9]{64}' , re.S | re.U | re.X ),
]

def hash( self, query ):
	if( query.ignored() ):
		return
	for regex in hash_regexes:
		result = analysis.emit( self.analysis, regex, query )
		if( result == True ):
			query.anomoly( 	Anomoly.HASH,
							trigger = regex.pattern,
							meta = result )

class iFuzzerAnomoly( iFuzzerComponent ):
	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def reflection( self, query, reflection ):
		"""
		Check for anomoly reflections
		"""
		pass

class FuzzerAnomoly( iFuzzerAnomoly, Strategy ):
	anomolies = Emitter()
	anomolies.on( Anomoly.DIFF, diff )
	anomolies.on( Anomoly.CODE, code )
	anomolies.on( Anomoly.TIME, time )
	anomolies.on( Anomoly.X_HEADER, x_header )
	anomolies.on( Anomoly.HASH, hash )
	X_CODES = [ 200, 404 ]
	X_CODES_EXTRA = [ 301, 304, 303, 307, 308, 500 ]
	X_HEADERS = {}
	X_TIME = 10

	def __init__( 	self, 
					bitmask = Anomoly.ALL, 
					anomoly = None, 
					reflection = None ):

		self.bitmask = Bitmask( bitmask )

		if( anomoly is None ):
			anomoly = FuzzerAnomoly._anomoly
		self.strategy( 'anomoly', anomoly )

		if( reflection is None ):
			reflection = FuzzerAnomoly._reflection
		self.strategy( 'reflection', reflection )

		self.x_codes = FuzzerAnomoly.X_CODES
		self.x_time = FuzzerAnomoly.X_TIME
		self.x_headers = FuzzerAnomoly.X_HEADERS
		self.context_check = False
		if( self.bitmask.mask() & Anomoly.CONTEXT == Anomoly.CONTEXT ):
			self.context_check = True
		self.analysis = self.bitmask.mask() & (Anomoly.SMART|Anomoly.FREQUENCY)
		self.filter = False
		if( self.bitmask.mask() & Anomoly.FILTER == Anomoly.FILTER ):
			self.filter = True

	def register_handlers( self, emitter ):
		emitter.on( Action.ANOMOLY, self.anomoly )

	def _anomoly( self, query ):
		logging.debug( '[ANOMOLY] %s' % query )
		for bit in self.bitmask:
			self.anomolies.emit( bit, self, query )
		if( self.context_check ):
			query.request.fuzzer.anomoly( query, self.analysis, self.filter )
		return query

	def reflection( self, query, reflection ): pass # abc compliance

	def _reflection( self, query, reflection ):
		logging.debug( '[ANOMOLY_REFLECTION] %s for %s' % ( reflection, query ))
		reflection.request.fuzzer = query.request.fuzzer
		self.anomoly( reflection )
		for anomoly in reflection.anomolies():
			query.reflection( reflection, anomoly )
