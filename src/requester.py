#!/usr/bin/env python

import abc

from patterns import Action, Type, Strategy, Event, BreakStream, Stream
from patterns import Priority
from ifuzzer import iFuzzerComponent

from query import Response

import time
import random

import logging
import traceback

from patterns import SilentBreakStream, Trigger

from copy import deepcopy

from requests.exceptions import Timeout
from requests.models import Response as RequestResponse

class Stealth( object ): # TODO
	PARANOID = 0x1
	SNEAKY = 0x2
	NORMAL = 0x3
	AGRESSIVE = 0x4
	INSANE = 0x5
	DEFAULT = NORMAL

class iFuzzerRequester( iFuzzerComponent ):

	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def register_queue( self, queue ):
		"""
		Register queue as self._job_queue
		"""
		pass

	@abc.abstractmethod
	def xsrf( self, query ):
		"""
		Resolve xsrf tokens on query
		"""
		pass

	@abc.abstractmethod
	def send( self, query ):
		"""
		Send request
		"""
		pass

	@abc.abstractmethod
	def setup( self ):
		"""
		Initialize variables dependant on other components
		"""
		pass

	@abc.abstractmethod
	def send_stack( self, query ):
		"""
		Send using query stack
		"""

	@abc.abstractproperty
	def pool_connections( self ): pass

	@abc.abstractproperty
	def max_poolsize( self ): pass

	@abc.abstractproperty
	def max_retries( self ): pass

class FuzzerRequester( iFuzzerRequester, Strategy ):
	def __init__( 	self,
					init = None,
					error = None,
					xsrf = None,
					send = None,
					queue = None,
					chain = None,
					max_retries = 2,
					max_poolsize = None,
					pool_connections = None,
					reflections = None ):
		if( xsrf is None ):
			xsrf = FuzzerRequester._xsrf
		self.strategy( 'xsrf', xsrf )
		if( error is None ):
			error = FuzzerRequester._error
		self.strategy( 'error', error )
		if( send is None ):
			send = FuzzerRequester._send
		self.strategy( 'send', send )
		if( queue is None ):
			queue = FuzzerRequester._queue
		self.strategy( 'queue', queue )
		if( chain is None ):
			chain = FuzzerRequester._chain
		self.strategy( 'chain', chain )
		if( reflections is None ):
			reflections = FuzzerRequester._reflections
		self.strategy( 'reflections', reflections )
		if( init is None ):
			init = FuzzerRequester._init
		self.strategy( 'init', init )
		self._job_queue = None
		self.wait = 0 # seconds to wait before request
		self.random_wait = None
		self._pool_connections = pool_connections
		self._max_poolsize = max_poolsize
		self._max_retries = max_retries

	# abc compliance

	def get_pool_connections( self ):
		return self._pool_connections
	def set_pool_connections( self, new ):
		self._pool_connections = new
	pool_connections = property( get_pool_connections, set_pool_connections )

	def get_max_poolsize( self ):
		return self._max_poolsize
	def set_max_poolsize( self, new ):
		self._max_poolsize = new
	max_poolsize = property( get_max_poolsize, set_max_poolsize )

	def get_max_retries( self ):
		return self._max_retries
	def set_max_retries( self, new ):
		self._max_retries = new
	max_retries = property( get_max_retries, set_max_retries )

	def send( self, query ): pass

	def xsrf( self, query ): pass

	#

	def register_handlers( self, emitter ):
		emitter.on( Action.REQUEST_QUEUE, self.queue )
		emitter.on( Action.REQUEST_SEND, self.send )
		emitter.on( Action.REQUEST_CHAIN, self.chain )
		emitter.on( Action.REQUEST_XSRF, self.xsrf )
		emitter.on( Action.REQUEST_REFLECTIONS, self.reflections )
		emitter.on( Action.REQUEST_INIT, self.init )

	def _queue( self, request ):
		logging.debug( '[REQUEST_QUEUE] %s' % request )
		if( self._job_queue is None ):
			raise TypeError( self._job_queue )
		self._job_queue.put( Event( Type.JOB, args=(request,)) )
		return request

	def _init( self, query ):
		logging.debug( '[REQUEST_INIT] %s' % query )
		return query

	def _error( self, query ):
		logging.debug( '[REQUEST_ERROR] %s' % query )

	def _send( self, query ):
		logging.debug( '[REQUEST_SEND] %s' % query )
		query.send()
		session = query.get_session()
		if( query.random_wait is not None ):
			time.sleep( random.uniform(	query.random_wait[0],
										query.random_wait[1] ))
		else:
			if( self.random_wait is not None ):
				time.sleep( random.uniform( self.random_wait[0],
											self.random_wait[1] ))
			else:
				if( query.wait != 0 ):
					time.sleep( query.wait )
				else:
					if( self.wait != 0 ):
						time.sleep( self.wait )
		start = time.time()
		timedout = False
		try:
			r = session.request( query.request.method, query.request.url,
					params = query.request.get,
					data = query.request.post,
					headers = query.request.headers,
					cookies = query.request.cookies,
					timeout = query.request.timeout,
					auth = query.request.auth,
					allow_redirects = query.request.allow_redirects,
					verify = query.request.verify,
					proxies = query.request.proxies,
					files = query.request.files
			)
		except Timeout:
			logging.debug( '[REQUEST_TIMEOUT] {}'.format( query ))
			r = RequestResponse()
			timedout = True
		except:
			self.error( query )
			stacktrace = traceback.format_exc()
			logging.debug( stacktrace )
			logging.error( '[REQUEST_FAILED] %s' % query )
			raise BreakStream()
		end = time.time()
		duration = end - start
		response = Response()
		response.code = r.status_code
		response.headers = r.headers
		response.proto = query.request.proto
		response.cookies = r.cookies
		response.body = r.text
		response.rtt = duration
		response.epoch = start
		if timedout:
			response.ignore = True
		query.recv( response )
		return query

	def _xsrf( self, query ):
		stack = []
		node = query.token 
		while( node is not None ):
			stack.append( node )
			node = node.token
		stack.reverse()
		for index, node in enumerate( stack ):

			self.fuzzer.transform.transmogrify( node )
			self.send( node )

			if( index > 0 ):
				node.tokenize()

		if( query.token is not None ):
			query.tokenize()

		return query

	def register_queue( self, queue ):
		self._job_queue = queue

	def setup( self ):
		logging.debug( '[REQUEST_SETUP]' )

		if( self.pool_connections is None ):
			self.pool_connections = self.fuzzer.pool.size()
		if( self.max_poolsize is None ):
			self.max_poolsize = self.fuzzer.pool.size()

	def _reflections( self, query ):
		logging.debug( '[REQUEST_REFLECTIONS] %s' % query )
		
		for ii, request in enumerate( query.request.reflections ):

			if( self.fuzzer.pool.free() > 0 
					and len(query.reflections()[ii:]) > 1 ):

				trigger = Trigger( len( query.reflections()[ii:] ))
				for reflection in query.reflections()[ii:]:
					self._job_queue.put( Event( Type.REFLECTION,
												args = ( 	trigger,
															query,
															reflection ) ))
				raise SilentBreakStream() # n-fork

			if( self.fuzzer.dispatcher.prefetch_reflection() ):
				# reflection prefetches are a bad idea to do here
				pass # self._prefetch_reflection( request ) # XXX
			self.send_stack( request )
			query.reflection_hook( request )

		for request in query.reflections():	# race condition fix via second loop
			self.fuzzer.anomoly.reflection( query, request )

		return query

	def _prefetch_reflection( self, reflection ):
		logging.debug( '[PREFETCH_REFLECTION] {}'.format( reflection ))
		prefetch = deepcopy( reflection )
		self.send_stack( prefetch )
		self.fuzzer.dispatcher.set_reflection_prefetch( reflection, prefetch )

	def _chain( self, query ):
		logging.debug( '[REQUEST_CHAIN] %s' % query )
		for request in query.request.chain:
			request.set_session( query.get_session() )
			self.send_stack( request )
		return query

	def send_stack( self, query ):
		self.fuzzer.transform.transmogrify( query )
		self.fuzzer.auth.init( query )
		self.fuzzer.auth.precheck( query )
		self.xsrf( query )
		self.send( query )
		self.fuzzer.auth.postcheck( query )
