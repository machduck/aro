#!/usr/bin/env python

from fuzzers.fuzz import Fuzz
from ifuzzer import iFuzzer, Scope
from patterns import Bitmask, flyweight
from random import sample
from query import Parameter
from fuzzers.data import Key 

@flyweight
class POSTFuzzer( iFuzzer ):
	type = Parameter.POST

	def __init__( self, bitmask = Fuzz.ALL, scope = (), exclude = () ):
		super( POSTFuzzer, self ).__init__()
		self.bitmask = Bitmask( bitmask )
		self.scope = Scope( scope )
		self.exclude = exclude

	def fuzz( self, resource ):
		params = []
		for key, value in resource.request.post.iteritems():
			if( key in self.scope and key not in self.exclude ):
				params.append( Key( self.type, key ))
		return iter( params )

	def random_fuzz( self, resource ):
		params = []
		for key, value in resource.request.post.iteritems():
			if( key in self.scope and key not in self.exclude ):
				params.append( Key( self.type, key ))
		return iter(sample( params, len( params )))
