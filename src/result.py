#!/usr/bin/env python

import abc

from patterns import Action, Type, Strategy
from ifuzzer import iFuzzerComponent

import logging

from Queue import Empty

class iFuzzerResult( iFuzzerComponent ):

	__metaclass__ = abc.ABCMeta

	def register_queue( self, queue ):
		"""
		Register queue as self._result_queue
		"""
		pass

class FuzzerResult( iFuzzerResult, Strategy ):
	def __init__( 	self, 
					anomoly = None,
					normal = None,
					filter = None,
					result = None,
					error = None ):

		if( normal is None ):
			normal = FuzzerResult._normal
		self.strategy( 'normal', normal )
		if( anomoly is None ):
			anomoly = FuzzerResult._anomoly
		self.strategy( 'anomoly', anomoly )
		if( filter is None ):
			filter = FuzzerResult._filter
		self.strategy( 'filter', filter )
		if( result is None ):
			result = FuzzerResult._result
		self.strategy( 'result', result )
		self._result_queue = None
		if( error is None ):
			error = FuzzerResult._error
		self.strategy( 'error', error )

	def register_handlers( self, emitter ):
		emitter.on( Action.RESULT, self.result )

	def register_queue( self, queue ):
		self._result_queue = queue

	def _error( self, response ):
		pass

	def _result( self, response ):
		response.freeze()
		if( self._result_queue is None ):
			raise TypeError( self._result_queue )
		if( self.filter( response )):
			self.anomoly( response )
		else:
			self.normal( response )
		return response

	def _filter( self, query ):
		if len( query.anomolies() ) > 0: 
			return True

	def _anomoly( self, response ):
		logging.info( '[!] Anomoly %s' % response )
		self.queue( response )

	def _normal( self, response ): 
		pass

	def queue( self, response ):
		self._result_queue.put( response )

	def size( self ):
		return self._result_queue.qsize()

	def results( self ):
		results = []
		try:
			while( True ):
				results.append( self._result_queue.get_nowait() )
		except Empty:
			return results
