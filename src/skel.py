#!/usr/bin/env python

from fuzzer import *

f = FuzzerFactory()

def normal( self, query ):
	logging.info( '[*] OK %s' % query )

def anomoly( self, query ):
	logging.warning( '[!] ANOMOLY %s' % query )
	self.queue( query )

def error( self, query ):
	logging.error( '[-] ERROR %s' % query )

f.result = FuzzerResult( anomoly = anomoly, normal = normal, error = error )
f.transformer = FuzzerTransformer( Transform.GOOGLE_CHROME )
f.dispatcher = FuzzerDispatcher( fetch = Fetch.ALL )
f.anomoly = FuzzerAnomoly( Anomoly.ALL | Anomoly.SMART | Anomoly.FILTER )

f.pool = FuzzerPool( 3 )

q1 = Query( url = 'php5fpm.nginx/sqli.php?one=1&two=two&three=3&four=4' )
q2 = Query( url = 'php5fpm.nginx/sqli2.php?one=1&two=two&three=3&four=4' )

fuzzer = f.create()
fuzzer.log( level = logging.DEBUG )

fuzzer.fuzz(dict(zip([q1,q2], [GETFuzzer( Fuzz.SQL )]*2 ))).join()

print '\n[*] +OK %d\n' % f.result.size()
for result in f.result.results():
	print '[*] %s' % result
	for anomoly in result.anomolies():
		print '[!] %s' % anomoly

#
#[*] +OK 2
#
#[*] <Query @http://php5fpm.nginx/sqli2.php GET one=1'>
#[!] <Detected SQLi @sqli [u'sqli']>
#[*] <Query @http://php5fpm.nginx/sqli2.php GET one=1">
#[!] <Detected SQLi @sqli [u'sqli']>
