#!/usr/bin/env python

import abc

import threading
from Queue import PriorityQueue, Queue

from patterns import Action, Type, Adapter, Event, Priority
from ifuzzer import iFuzzerComponent

import logging

class iFuzzerPool( iFuzzerComponent ):
	"""
	Fuzzer pool interface
	"""
	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def __init__( self, threads ):
		"""
		Constructor.

		threads : int
			Amount of threads to use
		"""
		pass

	@abc.abstractmethod
	def start( self ):
		"""
		Launch threads
		"""
		pass

	@abc.abstractmethod
	def stop( self ): 
		"""
		Stop threads
		"""
		pass

	@abc.abstractmethod
	def reactor( self, reactor ):
		"""
		Set reactor

		reactor : Reactor
		"""
		pass

	@abc.abstractmethod
	def size( self ):
		"""
		Return amount of threads
		"""
		pass

	@abc.abstractmethod
	def join( self ):
		"""
		Block until all threads are finished
		"""
		pass

	@abc.abstractmethod
	def free( self ):
		"""
		Retrun amount of freed threads
		"""
		pass

class FuzzerPool( iFuzzerPool ):
	def __init__( self, threads = 1 ):
		self._num = threads
		self._threads = []
		self._reactor = None
		self._killed = 0

	def start( self ):
		logging.debug( '[POOL] STARTING' )
		self._killed = 0
		def worker( reactor ):
			reactor.loop()
		if( self._reactor == None ):
			raise TypeError( self._reactor )
		for i in range( 0, self._num ):
			thread = threading.Thread( 	target = worker,
										args = ( self._reactor, ))
			thread.start()
			self._threads.append( thread )

	def reactor( self, reactor ):
		self._reactor = reactor

	def stop( self ):
		demux = self._reactor.demux()
		if( demux is Adapter ):
			demux = demux.adaptee()
		logging.debug( '[POOL] FREED THREAD %d' % self._killed )
		self._killed += 1
		if( self._killed == self._num ):
			for i in range( self._num ):
				logging.debug( '[POOL] STOPPING THREAD %d' % i )
				demux.put( Event( Action.STOP_REACTOR, Type.REACTOR ))
			logging.debug( '[POOL] STOPPED' )
			return Event( Action.STOP_REACTOR, Type.REACTOR )

	def register_handlers( self, emitter ):
		emitter.on( Action.STOP_REACTOR,
					lambda: Event( Action.STOP_REACTOR, Type.REACTOR ))

	def size( self ):
		return self._num

	def join( self ):
		logging.debug( '[POOL] JOINING' )
		for thread in self._threads:
			thread.join()

	def free( self ):
		return self._killed
