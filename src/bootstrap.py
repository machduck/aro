#!/usr/bin/python

import os
import pickle
import logging
from Queue import Queue, Empty
from collections import OrderedDict

from fuzzer import *

_results = Queue()

def _normal( self, query ):
	logging.info( '[*] OK %s' % query )

def _anomoly( self, query ):
	logging.warning( '[!] ANOMOLY %s' % query )
	_results.put( query )

def _error( self, query ):
	logging.error( '[-] ERROR %s' % query )

factory = FuzzerFactory()
factory.transformer = FuzzerTransformer( Transform.GOOGLE_CHROME )
factory.dispatcher = FuzzerDispatcher( fetch = Fetch.ALL )
factory.result = FuzzerResult( 	anomoly = _anomoly, 
								normal = _normal,
								error = _error )
factory.pool = FuzzerPool( 16 )
factory.anomoly = FuzzerAnomoly( Anomoly.ALL | Anomoly.SMART | Anomoly.FILTER )
fuzzer = factory.create()
fuzzer.log( level = logging.INFO )

class ResultsDict( OrderedDict ):
	def query( self, index ):
		return [(i,x) for (i,x) in enumerate(r.iterkeys())][index][1]

	def anomolies( self, index ):
		return [(i,x) for (i,x) in enumerate(r.itervalues())][index][1]

def loot():
	results = ResultsDict()
	print '[*] +OK %d' % _results.qsize()
	while( True ):
		try:
			result = _results.get_nowait() 
		except Empty:
			break
		results[ result ] = []
		print '[*] %s' % result
		for anomoly in result.anomolies():
			results[ result ].append( anomoly )
			print '[!] %s' % anomoly
	return results

def load( name ):
	path = os.path.abspath( os.path.expandvars( os.path.expanduser( name )))
	try: 
		queries = []
		for file in os.listdir( path ):
			filename = os.path.join( path, file )
			try:
				queries.append( load_file( filename ))
			except:
				logging.error( '[-] failed to import {}'.format( filename ))
		return queries
	except OSError:
		return load_file( path )

def load_file( file ):
	with open( file, 'rb' ) as fh:
		pickled = pickle.load( fh )
		logging.info( '[+] loaded {} from {}'.format( pickled, file ))
		return pickled

def fromfile( file ):
	path = os.path.abspath( os.path.expandvars( os.path.expanduser( file )))
	with open( path, 'rb' ) as fh:
		urls = [ x.strip() for x in fh.readlines()]
	return [ Query( url = x) for x in urls ]
