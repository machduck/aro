#!/usr/bin/env python

import logging

class Iterator( object ):
	"""
	Iterator
	"""
	def __init__( self, iterator, max = None ):
		self.max = max
		self.iterator = iterator

	def __iter__( self ):
		self.count = 0
		return self

	def next( self ):
		if( self.count == self.max ):
			raise StopIteration()
		self.count += 1
		return self.iterator( self.count-1 )

class Emitter( object ):
	"""
	Emitter
	"""
	def __init__( self ):
		self.actions = {};
		self._default = lambda *args, **kwargs: None

	def on( self, action, cb ):
		self.actions[ action ] = cb
		return self

	def off( self, action ):
		del self.actions[ action ]
		return self

	def emit( self, action, *args, **kwargs ):
		if( action not in self.actions ):
			return self._default( *args, **kwargs )
		else:
			return self.actions[ action ]( *args, **kwargs )

	def events( self ):
		return self.actions.keys()

	def default( self, cb ):
		self._default = cb
		return self

class Priority( object ):
	CRITICAL = 16
	HIGH = 32
	MEDIUM = 64
	LOW = 128
	LOWEST = 256

	DEFAULT = MEDIUM

class Event( Iterator ):
	"""
	Event
	"""
	def __init__( 	self, 
					name,
					type=None,
					args = (),
					kwargs = {},
					priority = Priority.DEFAULT ):

		self.name = name
		self.type = type
		self.args = args
		self.kwargs = kwargs
		self.priority = priority
		Iterator.__init__( self, self.iterate, 1 )

	def iterate( self, count ):
		return self.priority

	def __eq__( self, other ):
		if( self.name == other.name
				and self.type == other.type
				and self.priority == other.priority ):
			return True
		return False

	def __ne__( self, other ):
		return not self.__eq__( other )

	def __repr__( self ): return '<Event %s>' % Type.type.emit( self.name )

class Action( object ):
	STOP_REACTOR = 0x1

	AUTH_INIT = 0x2
	AUTH_PRECHECK = 0x3
	AUTH_POSTCHECK = 0x4
	TRANSFORM = 0x5
	REQUEST_QUEUE = 0x6
	REQUEST_CHAIN = 0x7
	REQUEST_XSRF = 0x8
	REQUEST_SEND = 0x9
	REQUEST_REFLECTIONS = 0xA
	REQUEST_INIT = 0xB
	ANOMOLY = 0xC
	RESULT = 0xD
	DISPATCH = 0xE
	DISPATCHER = 0xF
	TECH_DETECT = 0x10
	FINALLY = 0x11

	BREAK = 0x12

	TRANSMOGRIFY = 0x13

class Type( object ):
	REACTOR = 0x1

	JOB = 0x2
	JOB_REQUEST = 0x3

	STREAM = 0xA

	type = Emitter()
	type.default( lambda: '?' )
	type.on( REACTOR, lambda: 'REACTOR' )
	type.on( JOB, lambda: 'JOB' )
	type.on( JOB_REQUEST, lambda: 'JOB_REQUEST' )
	type.on( STREAM, lambda: 'STREAM' )

	REFLECTION = 0x6

	type.on( REFLECTION, lambda: 'REFLECTION' )

	PREFETCH = 0x7

	type.on( PREFETCH, lambda: 'PREFETCH' )

class Reactor( object ):
	"""
	Reactor
	"""
	STOP = Event( Action.STOP_REACTOR, Type.REACTOR )
	SIGNALS = [
		STOP,
	]

	def __init__( self, demux, loop = None ):
		self.emitters = []
		self._demux = demux
		if loop:
			self.loop = types.MethodType( loop, self )

	def register( self, emitter ):
		self.emitters.append( emitter )

	def remove( self, emitter ):
		self.emitters.remove( emitter )

	def select( self ):
		return self._demux.select()

	@property
	def size( self ):
		return self._demux.qsize()

	def event( self, event ):
		for emitter in self.emitters:
			r = emitter.emit( event.name, *event.args, **event.kwargs )
			self.task_done()
			if( r is not None and r in Reactor.SIGNALS ):
				return r

	def loop( self ):
		while True:
			event = self.select()
			logging.debug( '[REACTOR] %s' % event )
			r = self.event( event )
			if( r is not None  ):
				if( r == Reactor.STOP ):
					break

	def task_done( self ):
		self._demux.task_done()

	def demux( self ):
		return self._demux

class BreakStream( Exception ):	pass

class SilentBreakStream( Exception ): pass

class Stream( object ):
	"""
	Stream
	"""
	BREAK = Event( Action.BREAK, Type.STREAM )

	def __init__( self, args=() ):
		self.args = args

	def pipe( self, f, *args, **kwargs ):
		result = f( *( args + self.args ), **kwargs )
		if( result == None ):
			self.args = ()
			return self
		else:
			self.args = (result,)
			return self

	def reset( self ):
		self.args = ()
