#!/usr/bin/env python

import abc

from patterns import Action, Type, Strategy, Emitter, Generator, Bitmask
from ifuzzer import iFuzzerComponent

import math
import logging

class Obfuscation( object ): # TODO
	PARANOID = 0x1
	SNEAKY = 0x2
	NORMAL = 0x3
	AGRESSIVE = 0x4
	INSANE = 0x5
	DEFAULT = NORMAL

class Transform( object ):
	NONE = 0
	IPHONE = 1 << 1
	ANDROID = 1 << 2
	INTERNET_EXPLORER = 1 << 3
	GOOGLE_CHROME = 1 << 4
	IPAD = 1 << 5

def ipad( self, query ):
	ua = "Mozilla/5.0 (iPad; CPU OS 5_0 like Mac OS X) AppleWebKit/534.46 " \
			+ "(KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3"
	query.request.headers[ 'User-Agent' ] = ua

def iphone( self, query ):
	ua = "Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) " \
			+ "AppleWebKit/534.46 (KHTML, like Gecko) " \
			+ "Version/5.1 Mobile/9A334 Safari/7534.48.3"
	query.request.headers[ 'User-Agent' ] = ua

def android( self, query ):
	ua = "Mozilla/5.0 (Linux; U; Android 4.0.4; en-gb; " \
			+ "GT-I9300 Build/IMM76D) AppleWebKit/534.30 " \
			+ "(KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"
	query.request.headers[ 'User-Agent' ] = ua

def ie( self, query ):
	ua = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)"
	query.request.headers[ 'User-Agent' ] = ua

def google_chrome( self, query ):
	ua = "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 " \
			+ "(KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36"
	query.request.headers[ 'User-Agent' ] = ua

def sql_obfuscation( self, query ): pass # TODO

class FuzzerTransformer( iFuzzerComponent, Strategy ):
	transformations = Emitter()
	transformations.on( Transform.IPHONE, iphone )
	transformations.on( Transform.ANDROID, android )
	transformations.on( Transform.INTERNET_EXPLORER, ie )
	transformations.on( Transform.GOOGLE_CHROME, google_chrome )
	transformations.on( Transform.IPAD, ipad )

	def __init__( self, bitmask = 0, transform = None, transmogrify = None ):
		self.bitmask = Bitmask( bitmask )

		if( transform is None ):
			transform = FuzzerTransformer._transform
		self.strategy( 'transform', transform )

		if( transmogrify is None ):
			transmogrify = FuzzerTransformer._transmogrify
		self.strategy( 'transmogrify', transmogrify )

	def register_handlers( self, emitter ):
		emitter.on( Action.TRANSFORM, self.transform )
		emitter.on( Action.TRANSMOGRIFY, self.transmogrify )

	def _transmogrify( self, query ):
		logging.debug( '[TRANSMOGRIFY] %s' % query )
		for bit in self.bitmask:
			self.transformations.emit( bit, self, query )
		return query

	def _transform( self, query ):
		logging.debug( '[TRANSFORM] %s' % query )
		key, value = query.request.fuzzer.mutate( 	query.request.param.key,
													query.get( query \
																.request \
																.param ),
													query.request.word )
		query.update( query.request.param, key, value )
		self.transmogrify( query )
		return query
