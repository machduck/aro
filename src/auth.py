#!/usr/bin/env python

import abc

from patterns import Action, Strategy, lazyattr
from ifuzzer import iFuzzerComponent

import logging

from threading import Lock

from copy import deepcopy

from session import SessionManager, ContextSession, SessionsExhausted

try:
	from requests.adapters import HTTPAdapter
except ImportError:
	logging.error( '[IMPORT_FAILED] requests.adapters.HTTPAdapter' )

class iFuzzerAuth( iFuzzerComponent ):
	
	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def free_session( self, query ): 
		"""
		Put session from query back into session pool
		"""
		pass

	@abc.abstractmethod
	def init( self, query ):
		"""
		Create session for query
		"""
		pass

	@abc.abstractmethod
	def setup( self ):
		"""
		Initialize variables dependant on other components
		"""
		pass

	@abc.abstractmethod
	def precheck( self, query ): 
		"""
		Call login function if needed depending on query.doauth()
		"""
		pass

	@abc.abstractmethod
	def postcheck( self, query ):
		"""
		Call login function if needed depending on query.Response
		"""
		pass

class FuzzerAuth( iFuzzerAuth, Strategy ):
	def __init__( 	self,
					login = None,
					login_hook = None,
					logout = None,
					logout_hook = None,
					timeout = None,
					reset = True,
					context = False,
					precheck = None,
					postcheck = None,
					global_session = False ):

		self.is_session_global = global_session

		self.deepcopy_lock = Lock()

		self.sessions = None

		self.login_query = login
		self.logout_query = logout

		self.timeout = timeout
		self.reset = reset
		self.context = context

		if( login_hook is None ):
			login_hook = FuzzerAuth._login_hook
		self.strategy( 'login_hook', login_hook )

		if( logout_hook is None ):
			logout_hook = FuzzerAuth._logout_hook
		self.strategy( 'logout_hook', logout_hook )

		if( precheck is None ):
			precheck = FuzzerAuth._precheck_hook
		self.strategy( 'precheck_hook', precheck )

		if( postcheck is None ):
			postcheck = FuzzerAuth._postcheck_hook
		self.strategy( 'postcheck_hook', postcheck )

	def create_session( self, persistent = False ):
		return ContextSession( 	timeout = self.timeout,
								reset = self.reset, 
								persistent = persistent )

	def register_handlers( self, emitter ):
		emitter.on( Action.AUTH_PRECHECK, self.precheck )
		emitter.on( Action.AUTH_POSTCHECK, self.postcheck )
		emitter.on( Action.AUTH_INIT, self.init )

	def sane_check( self, request ):
		if( request.doauth() == True and self.login_query is None ):
			logging.warn( '[AUTH_NOT_FOUND] %s' % request )
			return False

		if( self.context
				and request.doauth() == False
				and self.logout_query is None ):

			logging.warn( '[DEAUTH_NOT_FOUND] %s' % request )
			return False
		
		return True

	def precheck( self, request ):
		logging.debug( '[AUTH_PRECHECK] %s' % request )

		if( self.sane_check( request ) ):

			if( request.doauth() == True
					and ( not request.get_session().isauthed()
							or request.get_session().doreauth() )):

				self.login( request )

			elif( self.context
					and request.doauth() is False
					and request.get_session().isauthed() ):

				self.logout( request )

		self.precheck_hook( request )
		return request

	def _precheck_hook( self, request ):
		logging.debug( '[AUTH_PRECHECK_HOOK] %s' % request )

	def postcheck( self, response ):
		logging.debug( '[AUTH_POSTCHECK] %s' % response )
		self.postcheck_hook( response )
		return response

	def _postcheck_hook( self, response ):
		logging.debug( '[AUTH_POSTCHECK_HOOK] %s' % response )

	def login( self, request ):
		logging.debug( '[AUTH_LOGIN] %s' % request )
		self.send( request, self.login_query )
		request.get_session().authed()
		logging.debug( '[ACCESS_GRANTED] %s' % request.get_session_cookies() )
		self.login_hook( request )

	def _login_hook( self, request ):
		logging.debug( '[AUTH_LOGIN_HOOK] %s' % request )

	def logout( self, request ):
		logging.debug( '[AUTH_LOGOUT] %s' % request )
		self.send( request, self.logout_query )
		request.get_session().deauthed()
		logging.debug( '[ACCESS_REVOKED] %s' % request.get_session_cookies() )
		self.logout_hook( request )

	def _logout_hook( self, request ):
		logging.debug( '[AUTH_LOGOUT_HOOK] %s' % request )

	def send( self, request_query, original_query ):
		self.deepcopy_lock.acquire()
		action_query = deepcopy( original_query )
		self.deepcopy_lock.release()
		action_query.set_session( request_query.get_session() )
		self.fuzzer.transform.transmogrify( action_query )
		self.fuzzer.request.xsrf( action_query )
		self.fuzzer.request.send( action_query )

	def init( self, query ):
		logging.debug( '[AUTH_INIT] %s' % query )
		query.set_session( self.get_session( query ) )
		return query

	def setup( self ):
		logging.debug( '[AUTH_SETUP]' )

		self.sessions = SessionManager()
		for x in range( self.fuzzer.pool.size() ):
			session = self.create_session( persistent = True )
			adapter = HTTPAdapter( 	max_retries=self.fuzzer.request.max_retries,
									pool_connections = self \
															.fuzzer \
															.request \
															.pool_connections,
									pool_maxsize = self \
														.fuzzer \
														.request \
														.max_poolsize )
			session.mount('http://', adapter )
			session.mount('https://',adapter )
			self.sessions.put( session )

	def free_session( self, query ):
		if( self.context or
				query.doauth()
				and query.get_session().ispersistent() ):
			logging.debug( '[SESSION_FREED] %s' % query.get_session_cookies() )
			self.sessions.put( query.get_session() )

	def get_session( self, query ):
		if( self.is_session_global ):
			return self.global_session
		if( self.context or query.doauth() == True ):
			try:
			 	return self.sessions.get( query )
			except SessionsExhausted:
				logging.warn( '[SESSIONS_EXHAUSTED]' )
				return self.create_session()
		else:
			return self.create_session()
	
	@lazyattr
	def global_session( self ):
		session = self.create_session()
		adapter = HTTPAdapter( 	max_retries=self.fuzzer.request.max_retries,
								pool_connections = self \
														.fuzzer \
														.request \
														.pool_connections,
								pool_maxsize = self \
													.fuzzer \
													.request \
													.max_poolsize )
		session.mount('http://', adapter )
		session.mount('https://',adapter )
		return session
