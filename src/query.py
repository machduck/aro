#!/usr/bin/env python

from patterns import Emitter, Strategy, SilentBreakStream

from werkzeug.exceptions import BadRequestKeyError

import logging

from anomoly import Detected, Reflected

from lxml import html
import re

from parameter import Parameter, CRUD
from request import Token, FuzzRequest, Request
from response import Response

from itertools import izip, chain as ichain

from bs4 import BeautifulSoup as soup

import os
import pickle
from copy import deepcopy, copy

from weakref import proxy

# get me out a pickle

import copy_reg
import types

def _pickle_method( method ):
	func_name = method.__func__.__name__
	obj = method.__self__
	cls = method.__self__.__class__
	return _unpickle_method, ( func_name, obj, cls )

def _unpickle_method( func_name, obj, cls ):
	for cls in cls.mro():
		try:
			func = cls.__dict__[ func_name ]
		except KeyError:
			pass
		else:
			break
	return func.__get__( obj, cls )

copy_reg.pickle( types.MethodType, _pickle_method, _unpickle_method )

class Query( Strategy ):
	setter = CRUD.setter
	getter = CRUD.getter
	updater = CRUD.updater
	deleter = CRUD.deleter

	REQUEST = Request

	def __init__( 	self,
					request = None, 
					token = None,
					token_hook = None,
					wait = 0,
					random_wait = None,
					**kwargs ):

		if( request is None ):
			self.request = self.REQUEST( **kwargs )
		else:
			self.request = request


		if( self.request.token_type is not None
				and self.request.token_key is not None ):
			self.deleter.emit( 	self.request.token_type, 
								self,
								self.request.token_key )
			self._expects_token = True

		self.token = token

		if( self.request.session is not None ):
			self.set_session( self.request.session )

		self.response = None

		self.wait = wait # seconds to wait before request
		self.random_wait = random_wait # min & max request wait time tuple

		if( token_hook is None ):
			token_hook = Query._token_hook
		self.strategy( 'token_hook', token_hook )

	def _token_hook( self ):
		logging.debug( '[XSRF_HOOK] %s' % self )
	
	def recv( self, response ):
		self.response = response

	def set( self, param, value ):
		self.setter.emit( param.type, self, param.key, value )

	def get( self, param ):
		return self.getter.emit( param.type, self, param.key )

	def delete( self, param ):
		self.deleter.emit( param.type, self, param.key )

	def update( self, param, key, value ):
		self.updater.emit( param.type, self, param.key, key, value )

	def build( self ):
		return self.request.proto.lower() + '://' \
				+ self.request.host \
				+ (lambda port: port if port != ':80' else '' ) \
						( ':' + str( self.request.port ) ) + '/' \
				+ '/'.join( self.request.path )

	def send( self ):
		self.request.url = self.build()

	def tokenize( self ):
		self.setter.emit( 	self.request.token_type,
							self,
							self.request.token_key,
							self.token.extract() )
		logging.debug( '[TOKEN] %s for %s' % ( self.get_token(), self ))
		self.token_hook()

	def get_token( self ):
		return self.token._token

	def expects_token( self ):
		return self._expects_token

	def set_session( self, session ):
		self.request.session = session
		if( self.token is not None ):
			self.token.set_session( self.request.session )

	def get_session( self ):
		return self.request.session

	def get_session_cookies( self ):
		if( self.get_session() is not None ):
			return self.get_session().cookies

class Extract( object ):
	XPATH = 'xpath'
	REGEX = 'regex'
	HEADER = 'header'
	COOKIE = 'cookie'

class TokenQuery( Query ):
	REQUEST = Token

	extractor = Emitter()
	extractor.on( 	Extract.XPATH.lower(),
					lambda self: html \
							.parse( self.response.body ) \
							.xpath( self.request.extract_key ))
	extractor.on( 	Extract.REGEX.lower(),
					lambda self: re \
							.compile( self.request.extract_key ) \
							.search( self.response.body ) \
							.group() )
	extractor.on( 	Extract.HEADER.lower(),
					lambda self:self.response.headers[self.request.extract_key])
	extractor.on( 	Extract.COOKIE.lower(),
					lambda self:self.response.cookies[self.request.extract_key])

	def __init__( self, *args, **kwargs ):
		Query.__init__( self, *args, **kwargs )
		self._token = None

	def extract( self ):
		try:
			self._token = self.extractor.emit( 	self.request.extract_type
														.lower(),
												self )
		except ( KeyError, BadRequestKeyError ):
			logging.error( '[TOKEN_NOT_FOUND] %s' % self  )
			logging.debug( '[TOKEN_NOT_FOUND] %s' % self.get_session_cookies() )
			raise SilentBreakStream()

		return self._token

	def __repr__( self ):
		return "<Token @%s %s %s=%s>" % ( 	self.build(),
											self.request.extract_type.lower(),
											self.request.extract_key,
											self._token or '?' )
	
class FuzzQuery( Query, Strategy ):
	REQUEST = FuzzRequest
	DOAUTH = None # override with wanted default value

	def __init__( 	self,
					request = None,
					token = None,
					token_hook = None,
					wait = 0,
					random_wait = None,
					reflection_hook = None,
					**kwargs ):

		Query.__init__( self,
						request = request,
						token = token,
						token_hook = token_hook,
						wait = wait,
						random_wait = random_wait,
						**kwargs )

		self._ismutable = True
		self._anomolies = [] # potential anomolies
		self.ratio = 0.950 	# simularity metric for pass/fail,
							# optionally set to self.request.volatility
		if( self.DOAUTH is not None and self.doauth() is None ):
			self.request.doauth = self.DOAUTH

		if( reflection_hook is None ):
			reflection_hook = FuzzQuery._reflection_hook
		self.strategy( 'reflection_hook', reflection_hook )

		self.fetch = None
	
	def _reflection_hook( self, reflection ):
		logging.debug( '[REFLECTION_HOOK] %s for %s' % ( reflection, self ))

	def anomoly( self, *args, **kwargs ):
		detected = Detected( *args, **kwargs )
		logging.debug( '[DETECT] %s for %s' % ( detected, self ))
		self._anomolies.append( detected )

	def reflection( self, *args, **kwargs ):
		reflected = Reflected( *args, **kwargs )
		logging.debug( '[REFLECT] %s for %s' % ( reflected, self ))
		self._anomolies.append( reflected )

	def anomolies( self ):
		return self._anomolies

	def reflections( self ):
		return self.request.reflections

	def doauth( self ):
		return self.request.doauth

	def params( self, type = None ):
		map = lambda map,key: izip( map.iteritems(), [key] * len(map.keys()) )
		for ((key,val),type) in ichain( map(self.request.cookies,
											Parameter.COOKIE ),

										map(self.request.headers,
											Parameter.HEADER ),

										map(self.request.get,
											Parameter.GET ),

										map(self.request.post,
											Parameter.POST) ):
			yield ( key, val, Parameter.type.emit( type ))
	
	def xrequest( self ):
		for key,val,type in self.params():
			print '{:<6} : {}={}'.format( type, key, val )

	def xresponse( self ):
		for header,value in self.response.headers.iteritems():
			print '{:<17} : {}'.format( header, value )

	def xbody( self ):
		print self.soup().prettify()
	
	def htmltofile( self, file = os.getcwd() ):
		path = os.path.abspath( os.path.expandvars( os.path.expanduser( file )))
		with open( path, 'wb' ) as fh:
			fh.write( self.soup().prettify().encode('utf-8') )
		logging.info( '[+] saved %s to %s' % ( self, path ))

	def vim( self, *args, **kwargs ):
		file = '/tmp/.aro'
		self.htmltofile( file = file )
		os.system( 'vim %s' % file )
		os.system( 'rm %s' % file )
		os.system( 'clear' )

	def prepickle( self ):
		self.request.session = None
		if( self.token is not None ):
			self.token.prepickle()
		for reflection in self.reflections():
			reflection.prepickle()

	def save( self, file = os.getcwd() ):
		path = os.path.abspath( os.path.expandvars( os.path.expanduser( file )))
		self.prepickle()
		with open( path, 'wb' ) as fh:
			pickle.dump( self, fh )
		logging.info( '[+] saved %s to %s' % ( self, file ))

	def soup( self ):
		return soup( self.response.body )

	def set_fuzzer( self, fuzzer ):
		self.request.fuzzer = proxy( fuzzer )

	def ismutable( self ):
		return self._ismutable

	def freeze( self ):
		self._ismutable = False

	def __repr__( self ):
		if( self.request.proto is None 
				or self.request.host is None
				or self.request.port is None
				or self.request.path is None ):
			return "<Query @None>"
		if( self.request.param is None
				or self.request.type is None ):
			return "<Query @%s>" % self.build()
		return "<Query @%s %s %s=%s>" % ( 	self.build(),
											Parameter.type.emit( self \
																	.request \
																	.type ),
											self.request.param.key,
											self.get( self.request.param ))

	def set_index( self, html ):
		self.request.index = html

	def set_rel_index( self, html ):
		self.request.rel_index = html

	def set_path( self, path = '' ):
		self.set_path_array(path.split( '/' ))

	def set_path_array( self, path = [] ):
		self.request.path = path

	def get_path( self ):
		return self.request.path

	def set_fetch( self, fetch ):
		self.fetch = fetch

	def get_fetch( self ):
		return self.fetch

	def get_html( self ):
		return self.response.body

	def set_not_found( self, html ):
		self.request.not_found = html

	def set_error( self, html ):
		self.request.error = html

	def set_null( self, html ):
		self.request.null = html

	def set_valid( self, html ):
		self.request.body = html

	def get_param( self ):
		return self.request.param

	def get_valid( self ):
		return self.request.body

	def ignored( self ):
		return self.response.ignore

	def get_path_array( self ):
		return self.request.path

	def rel_not_found( self, html ):
		sefl.request.rel_not_found = html
