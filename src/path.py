#!/usr/bin/env python

from fuzzers.fuzz import Fuzz
from ifuzzer import iFuzzer, Scope
from patterns import Bitmask, flyweight
from random import sample
from query import Parameter
from fuzzers.data import Key 

@flyweight
class PathFuzzer( iFuzzer ):
	type = Parameter.PATH

	def __init__( self, bitmask = Fuzz.ALL, scope = (), exclude = () ):
		super( PathFuzzer, self ).__init__()
		self.bitmask = Bitmask( bitmask )
		self.scope = Scope( scope )
		self.exclude = exclude

	def fuzz( self, resource ):
		params = []
		for index, value in enumerate(resource.request.path):
			if( value in self.scope and value not in self.exclude ):
				params.append( Key( self.type, index ))
		return iter( params )

	def random_fuzz( self, resource ):
		params = []
		for index, value in enumerate(resource.request.path):
			if( value in self.scope and value not in self.exclude ):
				params.append( Key( self.type, index ))
		return iter(sample( params, len( params )))
