#!/usr/bin/env python

import types
import math
import logging
from threading import Lock, Event as SyncEvent, Condition
from functools import wraps
import weakref

from reactor import Reactor, Event, Action, Type, Priority
from reactor import Emitter, Stream, BreakStream, SilentBreakStream
from reactor import Iterator

class Strategy( object ):
	"""
	Strategy
	"""
	def strategy( self, fn, strategy ):
		object.__setattr__( self,
							fn,
							types.MethodType( strategy, self ))

class Generator( object ):
	"""
	Generator
	"""
	def __init__( self, generator ):
		self.generator = generator

	def __iter__( self ):
		return self.generator()

class Adapter(object):
	"""
	Adapter
	"""
	def __init__(self, obj, adapted_methods):
		self.obj = obj
		self.__dict__.update(adapted_methods)

	def __getattr__(self, attr):
		return getattr(self.obj, attr)

	def adaptee( self ):
		return self.obj

class Bitmask( Generator ):
	"""
	Bitmask
	"""
	def __init__( self, bitmask = 0 ):
		self.bitmask = bitmask
		Generator.__init__( self, self.bits )

	def size( self ):
		return len([ x for x in self.bits() ])

	def bits( self ):
		if( self.bitmask == 0 ):
			return
		size = int( math.ceil( math.sqrt( self.bitmask )))
		for i in xrange(size+1): # +1 or skip 0b1
			cmask = ( 1 << size - i )
			if( self.bitmask & cmask == cmask ):
				yield( cmask )

	def mask( self ):
		return self.bitmask

	def __contains__( self, cmask ):
		return self.bitmask & cmask == cmask

class lazyattr(object):
	"""
	Lazy attribute decorator
	"""
	def __init__( self, f ):
		self.data = weakref.WeakKeyDictionary()
		self.f = f

	def __get__( self, obj, cls ):
		if obj not in self.data:
			self.data[ obj ] = self.f( obj )
		return self.data[ obj ]

def synced( lock = 'lock' ):
	"""
	Class sync
	"""
	def decorator( cls ):
		old_init = cls.__init__
		def new_init( self, *args, **kwargs ):
			old_init( self, *args, **kwargs )
			setattr( self, lock, Lock() )
		cls.__init__ = new_init
		return cls
	return decorator

def sync( lock = 'lock' ):
	"""
	Class method sync
	"""
	def decorator( f ):
		@wraps( f )
		def wrapper( self, *args, **kwargs ):
			with getattr( self, lock ):
				return f( self, *args, **kwargs )
		return wrapper
	return decorator

@classmethod
def _get_instance( cls, *args, **kwargs ):
	if( hasattr( cls, '__metaclass__' )):
		cls_type = cls.__metaclass__
	else:
		cls_type = cls
	return cls.__instances.setdefault( (args, tuple( kwargs.items() )),
										super( 	type(cls_type),
												cls_type ).__new__( *args, 
																	**kwargs ))

def flyweight( decoree ):
	"""
	Flyweight
	"""
	decoree.__instances = dict()
	decoree.__new__ = _get_instance
	return decoree

class Trigger( object ):
	def __init__( self, total ):
		self.lock = Lock()
		self.event = SyncEvent()
		self.counter = 0
		self.total = total

	def release( self ):
		with self.lock:
			self.counter += 1
			if( self.total == self.counter ):
				self.event.set()
				return True
			return False

class SyncTrigger( object ):
	def __init__( self, total = 0 ):
		self._default_total = total

		self.counter = 0
		self.total = total

		self.thread_lock = Lock()
		self.sync = SyncEvent()

		self.acquiring = SyncEvent()

	def release( self ):
		self.acquiring.wait()
		with self.thread_lock:
			logging.debug( '[TRIGGER] LOCK {}:{}'.format( self.counter, self.total )) # XXX
			self.counter += 1
			logging.debug( '[TRIGGER] INC {}:{}'.format( self.counter, self.total )) # XXX
			if( self.total == self.counter ):
				logging.debug( '[TRIGGER] TRIGGER {}:{}'.format( self.counter, self.total )) # XXX
				self.reset()
				self.sync.set()
				return True
			return False

	def wait( self ):
		self.acquiring.set()
		self.sync.wait()
		self.acquiring.clear()
		self.sync.clear()

	def reset( self ):
		self.counter = 0
		self.total = self._default_total

	def acquire( self ):
		with self.thread_lock:
			self.total += 1
			logging.debug( '[TRIGGER] ACQUIRE {}'.format( self.total )) # XXX
