#!/usr/bin/env python

from fuzzers.fuzz import Fuzz
from ifuzzer import iFuzzer
from patterns import Bitmask, flyweight
from random import sample
from query import Parameter
from fuzzers.data import Key 

from post import POSTFuzzer
from get import GETFuzzer
from header import HeaderFuzzer
from file import FileFuzzer
from cookie import CookieFuzzer
from path import PathFuzzer

from itertools import chain

@flyweight
class GenericFuzzer( iFuzzer ):
	type = Parameter.GENERIC

	def __init__( self, bitmask = Fuzz.ALL ):
		super( GenericFuzzer, self ).__init__()
		self.bitmask = Bitmask( bitmask )
		self.get = GETFuzzer( bitmask )
		self.post = POSTFuzzer( bitmask )
		self.header = HeaderFuzzer( bitmask )
		self.file = FileFuzzer( bitmask )
		self.cookie = CookieFuzzer( bitmask )
		self.path = PathFuzzer( bitmask )

	def fuzz( self, resource ):
		return chain( 	self.get.fuzz( resource ),
						self.path.fuzz( resource ),
						self.post.fuzz( resource ),
						self.cookie.fuzz( resource ),
						self.header.fuzz( resource ),
						self.file.fuzz( resource ))
						
	def random_fuzz( self, resource ):
		iterator = chain(
				self.get.random_fuzz( resource ),
				self.post.random_fuzz( resource ),
				self.cookie.random_fuzz( resource ),
				self.header.random_fuzz( resource ),
				self.path.random_fuzz( resource ),
				self.file.random_fuzz( resource ))
		return iterator
