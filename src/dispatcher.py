# -*- coding: utf-8 -*-

#!/usr/bin/env python

import abc

from patterns import Action, Strategy, Emitter, Adapter, Bitmask, Event, Type
from ifuzzer import iFuzzerComponent

from fuzzers.factory import FuzzerAbstractFactory
from copy import deepcopy

import random
import logging

from Queue import Empty

from parameter import  Parameter, CRUD
from patterns import Trigger, SyncTrigger

from functools import wraps
from weakref import WeakKeyDictionary

class Order( object ):

	RANDOM = 0b1

	PARAMETER = 0b0 # default ( second bit )
	FUZZ_STRING = 0b1 << 1

	QUERY = 0b0 # default ( third bit )
	FUZZER = 0b1 << 2

	DEFAULT = QUERY | PARAMETER

class Fetch( object ):
	NONE = 0
	ALL = 0b1111110

	ERROR = 0b1
	NOT_FOUND = 1 << 1
	NULL = 1 << 2
	VALID = 1 << 3
	INDEX = 1 << 4
	REL_INDEX = 1 << 5
	REL_NOT_FOUND = 1 << 6

class iFuzzerDispatcher( iFuzzerComponent ):

	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def fuzz( self, map ): pass

	@abc.abstractmethod
	def register_queues( self, job_queue, prefetch_queue ): pass

	@abc.abstractmethod
	def prefetch_reflection( self ): pass

	@abc.abstractmethod
	def set_reflection_prefetch( self ): pass

def random_inception( self, map ):
	factory = FuzzerAbstractFactory()
	self.map = {}
	for resource, fuzzer_group in map.items():
		self.map[ resource ] = {}
		for fuzzer in fuzzer_group:
			for param in fuzzer.random_fuzz( resource ):
				if( fuzzer.bitmask.mask() != 0 ):
					self.map[ resource ][ param ] = {}
					for bit in fuzzer.bitmask:
						fuzz = factory.create( bit )
						fuzz.register_tech( self.fuzzer.tech )
						self.map[resource][param][fuzz]=fuzz.random_words()

def random_dispatch( self ):
	while( True ):
		resource = random.choice( self.map.keys() )
		param = random.choice( self.map[ resource ].keys() )
		fuzz = random.choice( self.map[resource][param].keys() )
		word_generator = self.map[resource][param][fuzz]
		self.prefetch_query( resource ) # XXX
		self.prefetch_param( resource, param ) # XXX
		self.prefetch_merge( resource ) # XXX
		try:
			word = word_generator.next()
		except StopIteration:
			del self.map[resource][param][fuzz]
			if( len( self.map[resource][param].keys() ) == 0 ):
				del self.map[resource][param]
				if( len( self.map[ resource].keys() ) == 0 ):
					del self.map[resource]
					if( len(self.map.keys()) == 0 ):
						break
			continue

		copy = deepcopy( resource )
		copy.request.type = param.type
		copy.request.param = param
		copy.request.fuzzer = fuzz
		copy.request.word = word
		logging.debug( '[DISPATCHER] %s' % copy )
		yield( copy )

def query_iter_inception( self, map ):
	self.map = map
	self.factory = FuzzerAbstractFactory()

def query_param_dispatch( self ):
	for resource, fuzz_group in self.map.items():
		self.prefetch_query( resource ) # XXX
		for fuzzer in fuzz_group:
			for bit in fuzzer.bitmask:
				fuzz = self.factory.create( bit )
				fuzz.register_tech( self.fuzzer.tech )
				for param in fuzzer.fuzz( resource ):
					self.prefetch_param( resource, param ) # XXX
					self.prefetch_merge( resource ) # XXX
					for word in fuzz.words():
						copy = deepcopy( resource )
						copy.request.type = param.type
						copy.request.param = param
						copy.request.fuzzer = fuzz
						copy.request.word = word
						logging.debug( '[DISPATCHER] %s' % copy )
						yield( copy )

def query_word_dispatch( self ):
	for resource, fuzz_group in self.map.items():
		self.prefetch_query( resource ) # XXX
		for fuzzer in fuzz_group:
			for bit in fuzzer.bitmask:
				fuzz = self.factory.create( bit )
				fuzz.register_tech( self.fuzzer.tech )
				for word in fuzz.words():
					for param in fuzzer.fuzz( resource ):
						self.prefetch_param( resource, param ) # XXX
						self.prefetch_merge( resource ) # XXX
						copy = deepcopy( resource )
						copy.request.type = param.type
						copy.request.param = param
						copy.request.fuzzer = fuzz
						copy.request.word = word
						logging.debug( '[DISPATCHER] %s' % copy )
						yield( copy )

def fuzzer_iter_inception( self, map ):
	self.map = {}
	self.factory = FuzzerAbstractFactory()
	for resource, fuzzer_group in map.items():
		for fuzzer in fuzzer_group:
			for bit in fuzzer.bitmask:
				if( bit not in self.map.keys() ):
					self.map[bit] = {}
				if( resource not in self.map[bit].keys() ):
					self.map[bit][resource] = []
				self.map[bit][resource].append( fuzzer )

def fuzzer_param_dispatch( self ):
	for bit, rmap in self.map.items():
		fuzz = self.factory.create( bit )
		fuzz.register_tech( self.fuzzer.tech )
		for resource, fuzzer_group in rmap.items():
			self.prefetch_query( resource ) # XXX
			for fuzzer in fuzzer_group:
				for param in fuzzer.fuzz( resource ):
					self.prefetch_param( resource, param ) # XXX
					self.prefetch_merge( resource ) # XXX
					for word in fuzz.words():
						copy = deepcopy( resource )
						copy.request.type = param.type 
						copy.request.param = param
						copy.request.fuzzer = fuzz
						copy.request.word = word
						logging.debug( '[DISPATCHER] %s' % copy )
						yield( copy )

def fuzzer_word_dispatch( self ):
	for bit, rmap in self.map.items():
		fuzz = self.factory.create( bit )
		fuzz.register_tech( self.fuzzer.tech )
		for resource, fuzzer_group in rmap.items():
			self.prefetch_query( resource ) # XXX
			for fuzzer in fuzzer_group:
				for word in fuzz.words():
					for param in fuzzer.fuzz( resource ):
						self.prefetch_param( resource, param ) # XXX
						self.prefetch_merge( resource ) # XXX
						copy = deepcopy( resource )
						copy.request.type = param.type
						copy.request.param = param
						copy.request.fuzzer = fuzz
						copy.request.word = word
						logging.debug( '[DISPATCHER] %s' % copy )
						yield( copy )

def invoke_rel_index( self, query ):
	logging.debug( '[PREFETCH_REL_INDEX] {}'.format( query ))
	prefetch = deepcopy( query )
	prefetch.set_path_array( prefetch.get_path_array()[:-1] )
	prefetch.set_fetch( Fetch.REL_INDEX )
	self._job_queue.put( Event( Type.PREFETCH,
								args=( 	self._prefetch_sync,
										self._prefetch_queue,
										prefetch )))

def invoke_index( self, query ):
	logging.debug( '[PREFETCH_INDEX] {}'.format( query ))
	prefetch = deepcopy( query )
	prefetch.set_path()
	prefetch.set_fetch( Fetch.INDEX )
	self._job_queue.put( Event( Type.PREFETCH,
								args=( 	self._prefetch_sync,
										self._prefetch_queue,
										prefetch )))

def invoke_not_found( self, query ):
	logging.debug( '[PREFETCH_NOT_FOUND] {}'.format( query ))
	prefetch = deepcopy( query )
	prefetch.set_path( 'thisdoesnotexistlulz0x1337' )
	prefetch.set_fetch( Fetch.NOT_FOUND )
	self._job_queue.put( Event( Type.PREFETCH,
								args=( 	self._prefetch_sync,
										self._prefetch_queue,
										prefetch )))

def invoke_valid( self, query ):
	logging.debug( '[PREFETCH_VALID] {}'.format( query ))
	prefetch = deepcopy( query )
	prefetch.set_fetch( Fetch.VALID )
	self._job_queue.put( Event( Type.PREFETCH,
								args=( 	self._prefetch_sync,
										self._prefetch_queue,
										prefetch )))

def invoke_error( self, query, param ):
	logging.debug( '[PREFETCH_ERROR] {} {}'.format( query, param ))
	prefetch = deepcopy( query )
	CRUD.setter.emit( param.type, prefetch, param.key, self.INVOKE_ERROR )
	prefetch.set_fetch( Fetch.ERROR )
	self._job_queue.put( Event( Type.PREFETCH,
								args=( 	self._prefetch_sync,
										self._prefetch_queue,
										prefetch )))

def invoke_null( self, query, param ):
	logging.debug( '[PREFETCH_NULL] {} {}'.format( query, param ))
	prefetch = deepcopy( query )
	CRUD.setter.emit( param.type, prefetch, param.key, '' )
	prefetch.set_fetch( Fetch.NULL )
	self._job_queue.put( Event( Type.PREFETCH,
								args=( 	self._prefetch_sync,
										self._prefetch_queue,
										prefetch )))

class FuzzerDispatcher( iFuzzerDispatcher, Strategy ):
	INVOKE_ERROR =  \
			"""!@#$%^&*()-='"}{[].,?|=-_+~`""" + \
			"""<script>union""" + \
			"""\n\r""" + \
			"""%20%25%2525""" + \
			"""Xx1""" + \
			"""\\\\\\""" + \
			"""юЮ""" + \
			"""\\0\\x00%00""" + \
			"""%s%x%n""" + \
			"""AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"""

	inception = Emitter()
	inception.on( Order.QUERY | Order.PARAMETER, query_iter_inception )
	inception.on( Order.QUERY | Order.FUZZ_STRING, query_iter_inception )
	inception.on( Order.FUZZER | Order.PARAMETER, fuzzer_iter_inception )
	inception.on( Order.FUZZER | Order.FUZZ_STRING, fuzzer_iter_inception )
	inception.on( Order.RANDOM, random_inception )

	dispatch = Emitter()
	dispatch.on( Order.QUERY | Order.PARAMETER, query_param_dispatch )
	dispatch.on( Order.QUERY | Order.FUZZ_STRING, query_word_dispatch )
	dispatch.on( Order.FUZZER | Order.PARAMETER, fuzzer_param_dispatch )
	dispatch.on( Order.FUZZER | Order.FUZZ_STRING, fuzzer_word_dispatch )
	dispatch.on( Order.RANDOM, random_dispatch )

	retrieve_param = Emitter()
	retrieve_param.on( Fetch.NULL, invoke_null )
	retrieve_param.on( Fetch.ERROR, invoke_error )

	retrieve_query = Emitter()
	retrieve_query.on( Fetch.NOT_FOUND, invoke_not_found )
	retrieve_query.on( Fetch.VALID, invoke_valid )
	retrieve_query.on( Fetch.INDEX, invoke_index )
	retrieve_query.on( Fetch.REL_INDEX, invoke_rel_index )

	def __init__( 	self, 
					order = Order.DEFAULT,
					fetch = Fetch.NONE,
					dispatcher = None ):

		self.order = order
		self.fetch = Bitmask( fetch )
		self._job_queue = None

		if( dispatcher is None ):
			dispatcher = FuzzerDispatcher._dispatcher
		self.strategy( 'dispatcher', dispatcher )

		self._prefetch_sync = SyncTrigger()

	def register_handlers( self, emitter ):
		emitter.on( Action.DISPATCHER, self.dispatcher )

	def register_queues( self, job_q, prefetch_q ):
		self._job_queue = job_q
		self._prefetch_queue = prefetch_q

	def fuzz( self, map ):
		if( self._job_queue is None ):
			raise TypeError( self._job_queue )
		self.inception.emit( self.order, self, map )

	def _dispatcher( self ):
		generator = self.dispatch.emit( self.order, self )
		return Adapter( generator, { 'dispatch': generator.next })

	def prefetch_param( self, resource, param ):
		if( not self.doprefetch() ):
			return

		logging.debug( '[PREFETCH_PARAM] {} {}'.format( resource, param ))

		if( Fetch.ERROR in self.fetch ):
			self.retrieve_param.emit( Fetch.ERROR, self, resource, param )
			self._prefetch_sync.acquire()

		if( Fetch.NULL in self.fetch ):
			self.retrieve_param.emit( Fetch.NULL, self, resource, param )
			self._prefetch_sync.acquire()

	def prefetch_query( self, resource ):
		if( not self.doprefetch() ):
			return

		logging.debug( '[PREFETCH_QUERY] {}'.format( resource ))

		if( Fetch.NOT_FOUND in self.fetch ):
			self.retrieve_query.emit( Fetch.NOT_FOUND, self, resource )
			self._prefetch_sync.acquire()

		if( Fetch.VALID in self.fetch ):
			self.retrieve_query.emit( Fetch.VALID, self, resource )
			self._prefetch_sync.acquire()

	def doprefetch( self ):
		return self.fetch.mask() > 0 

	def prefetch_wait( self ):
		self._prefetch_sync.wait()

	def prefetch_merge( self, query ):
		if( not self.doprefetch() ):
			return

		self.prefetch_wait()

		prefetched = []

		while True:
			try:
				prefetched.append( self._prefetch_queue.get_nowait() )
			except Empty:
				break

		for prefetched_query in prefetched[:]:
			type = prefetched_query.get_fetch()
			html = prefetched_query.get_html()

			if type == Fetch.ERROR:
				query.set_error( html )

			elif type == Fetch.VALID:
				query.set_valid( html )

			elif type == Fetch.NULL:
				query.set_null( html )

			elif type == Fetch.NOT_FOUND:
				query.set_not_found( html )

			elif type == Fetch.INDEX:
				query.set_index( html )

			elif type == Fetch.REL_INDEX:
				query.set_rel_index( html )

			elif type == Fetch.REL_NOT_FOUND:
				query.set_rel_not_found( html )

	def prefetch_reflection( self ):
		if( self.fetch.mask() & Fetch.VALID == Fetch.VALID ):
			return True
		return False

	def set_reflection_prefetch( self, reflection, prefetch ):
		reflection.set_valid( prefetch.get_html() )
