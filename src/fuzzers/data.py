#!/usr/bin/env python

import abc
import os
import re
import json

from patterns import Emitter
from parameter import Parameter

import logging

class iData( object ):

	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def word( self, count ): pass

	@abc.abstractmethod
	def data( self ): pass

class SequenceNode( object ):
	def __init__( self, data, type ):
		self.data = data,
		self.type = type

def fuzzable( self, **kwargs ):
	self.name = kwargs['name']
	self.category = kwargs['category']
	self.tech = set(kwargs['tech'])
	self.level = kwargs['level']
	self.sequence = kwargs['sequence']
	self.key_mutation = kwargs['key_mutation'] # do NOT set to None on "none"
	self.value_mutation = kwargs['value_mutation']

	if( 'filter' in kwargs.keys() ):
		filter = kwargs['filter'].lower()
		if( filter == 'none' ):
			self.filter = None
		else:
			self.filter = filter
	elif( len( self.tech ) < 2 ):
		self.filter = None
	else:
		raise TypeError( self.filter )

	self.reflect = kwargs.get( 'reflect', None )

class Mods( object ):
	TIME_KEY = '[@TIME]'
	TIME_VAL = 15

	emitter = Emitter()
	emitter.default( lambda *args, **kwargs: None )
	emitter.on( 'time', lambda x: x.replace(str( Mods.TIME_KEY ), 
											str( Mods.TIME_VAL )))
	emitter.on( 'sql.char',
				lambda x: "".join(["char("+str(hex(ord(c)))+")" for c in x]))
	emitter.on( 'sql.hex',
				lambda x: "".join(['0x'+str(hex(ord(c))) for c in x ]))
	emitter.on( 'hex',
				lambda x: "".join([str(hex(ord(c))) for c in x]))

def component( self, **kwargs ):
	self.type = kwargs['type']
	self.data = kwargs['data']
	mod = kwargs.get( 'mod', None )
	if( mod is not None ):
		self.data = Mods.emitter.emit( mod, self.data )

class FuzzString( object ):
	emitter = Emitter()
	emitter.on( 'static', lambda x: x )

	init = Emitter()
	init.on( 'fuzzable', fuzzable )
	init.on( 'component', component )

	def __init__( self, **kwargs ):
		self.init.emit( kwargs['object'].lower(), self, **kwargs )
			
	def build( self ):
		return ''.join([self.emitter.emit( x.type.lower(), x.data )
						for x in self.sequence ])
	def __repr__( self ):
		return "<FuzzString %s> " % ( self.build() )

class JSONFuzzData( iData ):
	def __init__( self, file ):
		path = os.path.join( os.path.realpath( os.getcwd() ), 'data' )
		with open( os.path.join( path, file ), 'r' ) as fh:
			init = lambda kwargs: FuzzString( **kwargs )
			self.strings = json.load( fh, object_hook =  init )

	def word( self, count ):
		return self.strings[ count ]

	def data( self ):
		return self.strings

class DetectRegex( object ):
	emitter = Emitter()
	emitter.on( 'static', lambda detect: re.compile(re.escape( detect ),
													re.U | re.S | re.X ))
	emitter.on( 'regex', lambda detect: re.compile(detect, re.U | re.S | re.X))

	def __init__( self, **kwargs ):
		self.regex = self.emitter.emit( kwargs['type'], kwargs['data'] )
		self.type = kwargs['type']
		self.data = kwargs['data']
		self.tech = set(kwargs['tech'])
		self.category = kwargs['category']
		self.filter = kwargs['filter']

	def __repr__( self ):
		return "<DetectRegex %s:%s>" % ( self.data, self.type )

class JSONDetectData( iData ):
	def __init__( self, file ):
		path = os.path.join( os.path.realpath( os.getcwd() ), 'data' )
		with open( os.path.join( path, file ), 'r' ) as fh:
			init = lambda kwargs: DetectRegex( **kwargs )
			self.strings = json.load( fh, object_hook = init )

	def word( self, count ):
		return self.strings[ count ]

	def data( self ):
		return self.strings

class FileData( iData ):
	def __init__( self, file ):
		path = os.path.join( os.path.realpath( os.getcwd() ), 'data' )
		fh = open( os.path.join( path, file ), 'r' )
		self.lines = [ line.rstrip() for line in fh.readlines()
						if not line.startswith('#') ]
		fh.close()

	def word( self, count ):
		return self.lines[ count ]

	def data( self ):
		return self.lines

class RegexData( iData ):
	"""
	File data converted into regex without escaping
	"""
	def __init__( self, file ):
		path = os.path.join( os.path.realpath( os.getcwd() ), 'data' )
		fh = open( os.path.join( path, file ), 'r' )
		self.lines = [ re.compile( line.rstrip(), re.U | re.S | re.X )
						for line in fh.readlines()
						if not line.startswith('#') ]
		fh.close()

	def word( self, count ):
		return self.lines[ count ]

	def data( self ):
		return self.lines

class RegexFileData( iData ):
	"""
	File data escaped and converted into regex
	"""
	def __init__( self, file ):
		path = os.path.join( os.path.realpath( os.getcwd() ), 'data' )
		fh = open( os.path.join( path, file ), 'r' )
		self.lines = [ re.compile( 	re.escape( line.rstrip() ),
									re.U | re.S | re.X )
						for line in fh.readlines()
						if not line.startswith('#') ]
		fh.close()

	def word( self, count ):
		return self.lines[ count ]

	def data( self ):
		return self.lines

class Key( object ):
	def __init__( self, type, key ):
		self.type = type
		self.key = key

	def __repr__( self ):
		return '<{} {}>'.format( Parameter.type.emit( self.type ), self.key )

class Mutation( object ):
	def __init__( self, key = None, value = None ):
		self.key = key
		self.value = value
