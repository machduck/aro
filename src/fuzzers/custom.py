#!/usr/bin/env python

if __name__ == "__main__" and __package__ is None:
	from sys import path
	from os.path import dirname as dir
	path.append(dir(path[0]))

from fuzzers.fuzz import iFuzzGroup, Fuzz
from fuzzers.factory import FuzzerAbstractFactory
from patterns import Bitmask

class CustomFuzzer( iFuzzGroup ): # custom fuzzer grouping via bitmask
	def __init__( self, bitmask = Fuzz.NONE ):
		self.fuzzers = []
		bitmask = Bitmask( bitmask )
		factory = FuzzerAbstractFactory()
		for fuzzer in bitmask.bits():
			self.fuzzers.append( factory.create( fuzzer ))
	
	def register_tech( self, tech ):
		for fuzzer in self.fuzzers:
			fuzzer.register_tech( tech )

	def anomoly( self, query, method = None, filter = False ):
		for fuzzer in self.fuzzers:
			fuzzer.anomoly( query, method = method, filter = filter )
