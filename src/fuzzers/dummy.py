#!/usr/bin/env python

"""
!! DO NOT MODIFY DIRECTLY !!
1. COPY TO <FUZZERTYPE>.PY
2. CHANGE BITMASK
"""

if __name__ == "__main__" and __package__ is None:
	from sys import path
	from os.path import dirname as dir
	path.append(dir(path[0]))

from fuzzers.fuzz import Fuzz, iFuzz

from fuzzers.data import FileData, Mutation, RegexFileData
from fuzzers.data import RegexData
from fuzzers.data import JSONFuzzData, JSONDetectData

from patterns import Emitter

from tech import Level
from anomoly import Anomoly
from anomoly import analysis

import random
import re

class DummyFuzzer( iFuzz ):
	bitmask = Fuzz.DUMMY
	fuzz_file = 'dummy-fuzz.wl' # MODIFY
	detect_file = 'dummy-detect.wl' # MODIFY

	FUZZ = FileData( fuzz_file )
	DETECT = RegexFileData( detect_file )
		
	def __init__( self, level = Level.DEFAULT ):
		self.fuzz = self.FUZZ
		self.detect = self.DETECT

	def mutate( self, key, value, fuzzstring ):
		return ( str( key ), str( fuzzstring.value ) )

	def random_words( self ):
		mutations = []
		for word in random.sample( self.fuzz.data(), len(self.fuzz.data() )):
			mutations.append( Mutation( value = word ))
		return iter( mutations )

	def words( self ):
		mutations = []
		for word in self.fuzz.data():
			mutations.append( Mutation( value = word ))
		return iter( mutations )

	def anomoly( self, query, method = None, filter = False ):
		for regex in self.detect.data():
			result = analysis.emit( method, regex, query )
			if( result == True ):
				query.anomoly( 	Anomoly.DUMMY, # MODIFY
								trigger = regex.pattern,
								meta = result )
