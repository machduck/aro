#!/usr/bin/env python

if __name__ == "__main__" and __package__ is None:
	from sys import path
	from os.path import dirname as dir
	path.append(dir(path[0]))

from fuzzers.fuzz import Fuzz, iFuzz
from fuzzers.data import FileData, Mutation, RegexFileData

"""
For detection using regexes employ RegexData
(which does not escape regex special chars when reading from file)
instead of RegexFileData (which does escape).
"""
from fuzzers.data import RegexData

from tech import Level
from anomoly import Anomoly
from anomoly import analysis

import random
import re

# For advanced fuzzing use JSON format
from fuzzers.data import JSONFuzzData, JSONDetectData
# Emitter will probably be handy
from patterns import Emitter

class DemoFuzzer( iFuzz ):
	bitmask = Fuzz.DEMO
	fuzz_file = 'demo-fuzz.wl'
	detect_file = 'demo-detect.wl'

	FUZZ = FileData( fuzz_file )
	DETECT = RegexFileData( detect_file )
		
	def __init__( self, level = Level.DEFAULT ):
		self.fuzz = self.FUZZ
		self.detect = self.DETECT

	def mutate( self, key, value, fuzzstring ):
		return ( str( key ), str( fuzzstring.value ) )

	def random_words( self ):
		mutations = []
		for word in random.sample( self.fuzz.data(), len(self.fuzz.data() )):
			mutations.append( Mutation( value = word ))
		return iter( mutations )

	def words( self ):
		mutations = []
		for word in self.fuzz.data():
			mutations.append( Mutation( value = word ))
		return iter( mutations )

	def anomoly( self, query, method = None, filter = False ):
		for regex in self.detect.data():
			result = analysis.emit( method, regex, query )
			if( result == True ):
				query.anomoly( 	Anomoly.DEMO,
								trigger = regex.pattern,
								meta = result )
