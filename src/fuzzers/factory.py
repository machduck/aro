#!/usr/bin/env python

from fuzzers.fuzz import iFuzz

import straight.plugin

class FuzzerAbstractFactory( object ):
	fuzzers = {}
	plugin_manager = straight.plugin.load( 'fuzzers', subclasses = iFuzz )
	for plugin in plugin_manager:
		if( plugin.bitmask in fuzzers.keys() ):
			raise ValueError( plugin.bitmask )
		fuzzers[ plugin.bitmask ] = plugin

	def has( self, bit ):
		return self.fuzzers.has_key( bit )

	def create( self, bit, *args, **kwargs ):
		return self.fuzzers[bit]( *args, **kwargs )
