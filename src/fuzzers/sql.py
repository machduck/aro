#!/usr/bin/env python

if __name__ == "__main__" and __package__ is None:
	from sys import path
	from os.path import dirname as dir
	path.append(dir(path[0]))

from fuzzers.fuzz import Fuzz, iFuzz
from fuzzers.data import FileData, Mutation, RegexFileData, RegexData
from fuzzers.data import JSONFuzzData, JSONDetectData
from patterns import Emitter
from anomoly import Anomoly
from tech import Level
from anomoly import analysis

import random
import re

class SqlFuzzer( iFuzz ):
	bitmask = Fuzz.SQL
	detect_file = 'sql-detect.json'
	fuzz_file = 'sql-fuzz.json'

	key_mutate = Emitter()
	key_mutate.on( 'none', lambda key: key )

	value_mutate = Emitter()
	value_mutate.on( 'append', lambda old, new: "%s%s" % ( old, new ))
	value_mutate.on( 'overwrite', lambda old, new: new )
	value_mutate.on( 'prepend', lambda old, new: "%s%s" % ( new, old ))

	FUZZ = JSONFuzzData( fuzz_file )
	DETECT = JSONDetectData( detect_file )

	def __init__( self, stealth = Level.DEFAULT ):
		self.fuzz = self.FUZZ
		self.detect = self.DETECT
		self.stealth = stealth

	def mutate( self, key, value, fuzzstring ):
		k = self.key_mutate.emit( fuzzstring.key_mutation, key )
		v = self.value_mutate.emit(	fuzzstring.value_mutation, 
									value,
									fuzzstring.build() )
		return ( str( k ), str( v ) )

	def random_words( self ):
		mutations = []
		for word in random.sample( self.fuzz.data(), len( self.fuzz.data() )):
			if( not self.tech.allow( word ) ):
				continue
			mutations.append( word )
		return iter( mutations )

	def words( self ):
		mutations = []
		for word in self.fuzz.data():
			if( not self.tech.allow( word )):
				continue
			mutations.append( word )
		return iter( mutations )

	def anomoly( self, query, method = None, filter = False ):
		for detect in self.detect.data():
			result = analysis.emit( method, detect.regex, query )
			if( result == True ):
				query.anomoly( 	Anomoly.SQL,
								trigger = detect.regex.pattern,
								meta = result )
				self.tech.update( detect, filter )
