#!/usr/bin/env python

import abc

class iFuzzBase( object ):

	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def anomoly( self, query ):
		"""
		Check query for anomolies caused by self and mark if found
		"""
		pass

class iFuzzGroup( iFuzzBase ):

	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def register_tech( self, tech ):
		"""
		Register tech with fuzzers
		"""
		pass

class iFuzz( iFuzzBase ):

	__metaclass__ = abc.ABCMeta

	@abc.abstractproperty
	def bitmask( self ): 
		"""
		Bitmask in Fuzz class used to identify fuzzer
		"""
		pass

	@abc.abstractmethod
	def mutate( self, param, permutation ):
		"""
		Return final mutated value

		return: string
		"""
		pass

	@abc.abstractmethod
	def random_words( self ):
		"""
		Return an iterator to a randomized list of values to fuzz
		"""
		pass

	@abc.abstractmethod
	def words( self ):
		"""
		Return an iterator to a list of values to fuzz
		"""

	def register_tech( self, tech ):
		self.tech = tech

class Fuzz( object ):
	ALL = 0b100
	NONE = 0
	DUMMY = 0

	DEMO = 1 << 1

	SQL = 1 << 2

	XSS = 1 << 3

	LDAP = 1 << 4

	XXE = 1 << 5
	XPATH = 1 << 6

	LFI = 1 << 7
	RFI = 1 << 8

	OS_CMD = 1 << 9

	BITFLIP = 1 << 10

	OPENDIR = 1 << 11
	AUTH_BYPASS = 1 << 12

	CSRF = 1 << 13
	RESPONSE_SPLITTING = 1 << 14

	PHP_INCLUDE = 1 << 15

	#SQL_UNION = 0b1
	#SQL_ERROR = 1 << 1
	#SQL_INSERT = 1 << 2
	#SQL_UPDATE = 1 << 3
	#SQL_TIME = 1 << 4
	#SQL_BLIND = 1 << 5
	#SQL_LFI = 1 << 6
	#SQL_MULTI_QUERY = 1 << 7
	#SQL_SECOND_ORDER = 1 << 8
	#SQL_FILE_UPLOAD = 1 << 9
	#SQL_INSERT_ON_DUPLICATE_KEYS = 1 << 10
	#SQL_CMD = 1 << 11
	#SQL = SQL_UNION \
			#| SQL_ERROR \
			#| SQL_INSERT \
			#| SQL_UPDATE \
			#| SQL_TIME \
			#| SQL_BLIND \
			#| SQL_LFI \
			#| SQL_INSERT_ON_DUPLICATE_KEYS \
			#| SQL_CMD \
			#| SQL_MULTI_QUERY \
			#| SQL_FILE_UPLOAD \
			#| SQL_SECOND_ORDER \

	#XSS_PASSIVE = 0b1
	#XSS_ACTIVE = 1 << 1
	#XSS_DOM = 1 << 2
	#XSS = XSS_ACTIVE | XSS_PASSIVE | XSS_DOM

	#MONGODB = 0b1
	#SSJI = 0b1
	#JSON_P = 0b1

	#PHP_UNSERIALIZE = 0b1
	#PHP_MONGODB = 0b1
	#PHP_REG_GLOBALS = 0b1

	#SSI = 0b1
	#BASICAUTH_BYPASS = 0b1

	#XSLT = 0b1
	#XQUERY = 0b1
