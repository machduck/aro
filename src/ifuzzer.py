#!/usr/bin/env python

import abc

from patterns import Stream, Iterator

class Scope( tuple ):
	def __contains__( self, item ):
		if( len(self) == 0 ):
			return True
		else:
			return tuple.__contains__( self, item )

class FuzzGroup( Iterator ):
	def __init__( self, *args ):
		self.fuzzers = args
		Iterator.__init__( self, self.group, len( self.fuzzers ) )

	def register_fuzzer( self, *args, **kwargs ):
		for fuzzer in self.fuzzers:
			fuzzer.register_fuzzer( *args, **kwargs )

	def group( self, count ):
		return self.fuzzers[ count ]

class FuzzAlone( Iterator ):
	def __init__( self ):
		Iterator.__init__( self, self.group, 1 )

	def register_fuzzer( self, fuzzer ):
		self.fuzzer = fuzzer

	def group( self, count ):
		return self

class iFuzzer( FuzzAlone ):

	__metaclass__ = abc.ABCMeta

	def __init__( self ):
		self.stream = Stream()
		super( iFuzzer, self ).__init__()

	@abc.abstractproperty
	def type( self ):
		"""
		query.Query.Parameter
		"""
		pass

	@abc.abstractmethod
	def fuzz( self, resource ):
		"""
		Return iterator to fuzzable values

		resource : Query
		"""
		pass
	@abc.abstractmethod
	def random_fuzz( self, resource ):
		"""
		Return an iterator to a randomized list of fuzzable values

		resource : Query
		"""
		pass

class iFuzzerComponent( object ):

	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def register_handlers( self, emitter ):
		"""
		Register component callbacks with emitter

		emitter : Emitter
		"""
		pass

	def register_fuzzer( self, fuzzer ):
		self.fuzzer = fuzzer
