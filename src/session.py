#!/usr/bin/env python

import logging
import time

from patterns import Emitter, sync, synced

from requests import Session

class AuthedSession( Session ):
	NEVER_EXPIRES = 0xdead1337
	translator = Emitter()
	translator.on( 's', lambda x: x )
	translator.on( 'm', lambda x: x*60 )
	translator.on( 'h', lambda x: x*60*60 )
	translator.on( 'd', lambda x: x*60*60*24 )

	def __init__( 	self,
					persistent = False,
					timeout = None,
					reset = True,
					*args, **kwargs ):

		Session.__init__( self, *args, **kwargs )
		if( timeout is not None ):
			if( isinstance( timeout, int )):
				self.timeout = timeout
			elif( timeout.isdigit() ): # raises TypeError if not string
				self.timeout = int( timeout )
			else:
				type = timeout[-1]
				duration = int(timeout[:-1])
				self.timeout = self.translator.emit( type, duration )
		else:
			self.timeout = None
		self.reset = reset
		self.start_time = time.time()
		self.delta = 5 # XXX 5 seconds for request to reach server after check
		self.persistent = persistent

	def ispersistent( self ):
		return self.persistent

	def doreauth( self ):
		if( self.timeout is None ):
			return False # no set timeout is equivilent of never expires
		now = time.time()
		if( now + self.delta - self.start_time > self.timeout ):
			self.start_time = now
			return True
		if( self.reset ):
			self.start_time = now
		return False

	def expires( self ):
		if( self.timeout is None ):
			timeout = self.NEVER_EXPIRES
		else:
			timeout = self.timeout
		return timeout - ( time.time() - self.start_time + self.delta )

class ContextSession( AuthedSession ):
	def __init__( self, *args, **kwargs ):
		AuthedSession.__init__( self, *args, **kwargs )
		self._authed = False

	def isauthed( self ):
		return self._authed

	def authed( self ):
		self._authed = True

	def deauthed( self ):
		self._authed = False

	def __repr__( self ):
		return '<Session %ds>' % self.expires()

class SessionsExhausted( Exception ): pass

@synced()
class SimpleSessionManager( list ):
	def __init__( self, *args, **kwargs ):
		list.__init__( self, *args, **kwargs )

	@sync()
	def get( self ):
		if( len( self ) == 0 ):
			raise SessionsExhausted()
		return self.pop()

	@sync()
	def put( self, node ):
		self.append( node )

@synced()
class SessionManager( list ):
	"""
	Session manager with sessions sorted from oldest to newest
	"""
	def __init__( self, *args, **kwargs ):
		list.__init__( self, *args, **kwargs )
		self.expired_border = 0 # index to oldest alive session

	@sync()
	def get( self, query ):

		logging.debug( '[SESSIONS] %s' % self )

		if( len( self ) == 0 ):
			raise SessionsExhausted()

		if( self[-1].expires() < 0 ):
			# if most fresh session is expired
			# all older sessions are expired
			logging.debug( '[SESSIONS_EXPIRED]' )
			if( self.expired_border > 0 ):
				self.expired_border -= 1
			return self.pop()

		for session in self[ self.expired_border: ]:
			if( session.expires() >= 0 ):
				if( query.doauth() is None
						or query.doauth() == session.isauthed() ):
					# if session has requested privs
					# if session has not expired
					logging.debug( '[SESSION] %s' % session )
					self.remove( session )
					return session
			else:
				self.expired_border += 1

		# return oldest alive session if no privs matched requested
		if( self.expired_border > 0 ):
			self.expired_border -= 1
		session = self[ self.expired_border ]
		logging.debug( '[WRONG_PRIVILEGES] %s' % session )
		self.remove( session )
		return session

	@sync()
	def put( self, node ):
		self.append( node )
