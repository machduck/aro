#!/usr/bin/env python

from patterns import Emitter

class Parameter( object ):
	GET = 0x1
	POST = 0x2
	PATH = 0x3
	COOKIE = 0x4
	HEADER = 0x5
	FILE = 0x6

	GENERIC = 0x0

	type = Emitter()
	type.on( GET, lambda: 'GET' )
	type.on( POST, lambda: 'POST' )
	type.on( PATH, lambda: 'PATH' )
	type.on( COOKIE, lambda: 'COOKIE' )
	type.on( HEADER, lambda: 'HEADER' )
	type.on( FILE, lambda: 'FILE' )

def set_get( self, key, value ):
	self.request.get[ key ] = value

def set_post( self, key, value ):
	self.request.post[ key ] = value

def set_path( self, key, value ):
	self.request.path[ key ] = value

def set_cookie( self, key, value ):
	self.request.cookies[ key ] = value

def set_header( self, key, value ):
	self.request.headers[ key ] = value

def set_file( self, key, value ):
	self.reqeust.files[ key ] = value

def get_get( self, key ):
	return self.request.get[ key ]

def get_post( self, key ):
	return self.request.post[ key ]

def get_path( self, key ):
	return self.request.path[ key ]

def get_cookie( self, key ):
	return self.request.cookies[ key ]

def get_header( self, key ):
	return self.reqeust.headers[ key ]

def get_file( self, key ):
	return self.reqeust.files[ key ]

def del_get( self, key ):
	del self.request.get[ key ]

def del_post( self, key ):
	del self.request.post[ key ]

def del_path( self, key ):
	del self.request.path[ key ]

def del_cookie( self, key ):
	del self.request.cookies[ key ]

def del_header( self, key ):
	del self.request.headers[ key ]

def del_file( self, key ):
	del self.reqeust.files[ key ]

def update_get( self, key, new_key, value ):
	self.request.get[ new_key ] = value
	if( key != new_key ):
		del self.request.get[ key ]

def update_post( self, key, new_key, value ):
	self.request.post[ new_key ] = value
	if( key != new_key ):
		del self.request.post[ key ]

def update_path( self, key, new_key, value ):
	self.request.path[ int(new_key) ] = value

def update_cookie( self, key, new_key, value ):
	self.request.cookies[ new_key ] = value
	if( key != new_key ):
		del self.request.cookies[ key ]

def update_header( self, key, new_key, value ):
	self.request.headers[ new_key ] = value
	if( key != new_key ):
		del self.request.headers[ key ]

def update_file( self, key, new_key, value ):
	self.request.files[ new_key ] = value
	if( key != new_key ):
		del self.request.files[ key ]

class CRUD( object ):
	setter = Emitter()
	setter.on( Parameter.GET, set_get )
	setter.on( Parameter.POST, set_post )
	setter.on( Parameter.PATH, set_path )
	setter.on( Parameter.COOKIE, set_cookie )
	setter.on( Parameter.HEADER, set_header )
	setter.on( Parameter.FILE, set_file )

	getter = Emitter()
	getter.on( Parameter.GET, get_get )
	getter.on( Parameter.POST, get_post )
	getter.on( Parameter.PATH, get_path )
	getter.on( Parameter.COOKIE, get_cookie )
	getter.on( Parameter.HEADER, get_header )
	getter.on( Parameter.FILE, get_file )

	deleter = Emitter()
	deleter.on( Parameter.GET, del_get )
	deleter.on( Parameter.POST, del_post )
	deleter.on( Parameter.PATH, del_path )
	deleter.on( Parameter.COOKIE, del_cookie )
	deleter.on( Parameter.HEADER, del_header )
	deleter.on( Parameter.FILE, del_file )

	updater = Emitter()
	updater.on( Parameter.GET, update_get )
	updater.on( Parameter.POST, update_post )
	updater.on( Parameter.PATH, update_path )
	updater.on( Parameter.COOKIE, update_cookie )
	updater.on( Parameter.HEADER, update_header )
	updater.on( Parameter.FILE, update_file )
