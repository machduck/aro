#!/usr/bin/env python

class Response( object ):
	def __init__( 	self, 
					code = None,
					body = None,
					headers = {},
					cookies = {},
					rtt = None,
					epoch = None,
					ignore = False ):

		self.headers = headers or {}
		self.body = body or None
		self.cookies = cookies or {}
		self.code = code or None
		self.rtt = rtt or None
		self.epoch = epoch or None
		self.ignore = ignore or False
