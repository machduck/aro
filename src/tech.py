#!/usr/bin/env python

from patterns import Action, Type, Strategy, Emitter, Bitmask
from ifuzzer import iFuzzerComponent

import logging

class Level( object ):
	PARANOID = 0x1
	SNEAKY = 0x2
	NORMAL = 0x3
	AGRESSIVE = 0x4
	INSANE = 0x5
	DEFAULT = NORMAL

class Tech( object ):
	ALL = 0b11111111111111
	NONE = 0
	DEFAULT = ALL

	LINUX = 0b1
	WINDOWS = 1 << 1

	NGINX = 1 << 2
	APACHE = 1 << 3
	TOMCAT = 1 << 4
	IIS = 1 << 5

	PHP = 1 << 6

	PYTHON = 1 << 7
	DJANGO = 1 << 8
	FLASK = 1 << 9

	RUBY = 1 << 10
	ROR = 1 << 11

	NODEJS = 1 << 12
	EXPRESS = 1 << 13

def linux( self, query ): pass # TODO

def windows( self, query ): pass # TODO

def update_blacklist( self, detect ):
	self.blacklist( detect.category, detect.tech )

def update_whitelist( self, detect ):
	self.whitelist( detect.category, detect.tech )

class FuzzerTech( iFuzzerComponent, Strategy ):
	tech = Emitter()
	tech.on( Tech.LINUX, linux )
	tech.on( Tech.LINUX, windows )
	updater = Emitter()
	updater.on( 'whitelist', update_whitelist )
	updater.on( 'blacklist', update_blacklist )

	def __init__( 	self, 
					bitmask = Tech.DEFAULT,
					level = Level.DEFAULT,
					whitelist = {},
					blacklist = {},
					detect = None ):

		self.bitmask = Bitmask( bitmask )
		self._level = level
		self._blacklist = blacklist
		self._whitelist = whitelist
		for key, value in self._blacklist.iteritems():
			self._blacklist[key] = set( value )
		for key, value in self._whitelist.iteritems():
			self._whitelist[key] = set( value )
		if( detect is None ):
			detect = FuzzerTech._detect
		self.strategy( 'detect', detect )

	def _detect( self, query ):
		for bit in self.bitmask:
			self.tech.emit( bit, self, query )
		return query

	def register_handlers( self, emitter ):
		emitter.on( Action.TECH_DETECT, self.detect )

	def level( self, level = None ):
		if( level is None ):
			return self._level
		self._level = level

	def whitelist( self, category = None, tech = None ):
		if( tech is not None and category is not None ):
			if( category not in self._whitelist.keys() ):
				self._whitelist[ category ] = set()
			self._whitelist[ category ].update( tech )
			logging.debug( '[WHITELIST] [+] %s' % tech )
		return self._whitelist

	def blacklist( self, category = None, tech = None ):
		if( tech is not None and category is not None ):
			if( category not in self._blacklist.keys() ):
				self._blacklist[ category ] = set()
			logging.debug( '[BLACKLIST] [+] %s' % tech )
			self._blacklist[ category ].update( tech )
		return self._blacklist

	def is_whitelist( self ):
		return bool( len( self.whitelist().keys() ))

	def is_blacklist( self ):
		return bool( len( self.blacklist().keys() ))

	def allow_whitelist( self, word ):
		if( self.is_whitelist() ):
			if( word.category in self.whitelist().keys() 
					or word.category.lower() == 'generic' ):
				if( word.filter == None or word.filter.lower() == 'or' ):
					# if not 1+ elements in whitelist -> fail
					if(	not word.tech \
							.isdisjoint((self.whitelist())[word.category])
							or 'generic' in [ x.lower() for x in word.tech ] ):
						return True
					logging.debug( '[FAILED] @whitelist OR %s' % word )
					return False
				elif( word.filter.lower() == 'and' ):
					# if not all elements in whitelist -> fail
					if(	word.tech \
							.issubset((self.whitelist())[word.category])
							or 'generic' in [ x.lower() for x in word.tech ] ):
						return True
					logging.debug( '[FAILED] @whitelist AND %s' % word )
					return False
				else:
					logging.debug('[FAILED] @whitelist UNKNOWN %s' % word )
					return False
		return True

	def allow_blacklist( self, word ):
		if( self.is_blacklist() ):
			if( word.category in self.blacklist().keys() ):
				if( word.filter is None or word.filter.lower() == 'or' ):
					# if 1+ element in blacklist -> fail
					if( not word.tech \
							.isdisjoint((self.blacklist())[word.category])):
						logging.debug( '[FAILED] @blacklist OR %s' % word )
						return False
				elif( word.filter.lower() == 'and' ):
					# if all elements in blacklist -> fail
					if( word.tech \
							.issubset(((self.blacklist())[word.category]))):
						logging.debug( '[FAILED] @blacklist AND %s' % word )
						return False
				else:
					logging.debug( '[FAILED] @blacklist UNKNOWN %s' % word )
					return False
		return True

	def allow( self, word ):
		logging.debug( '[PENDING] %s' % word)
		if( word.level > self.level() ):
			logging.debug( '[FAILED] @level %s' % word )
			return False

		if( self.allow_whitelist( word ) and self.allow_blacklist( word ) ):
			logging.debug( '[PASSED] %s' % word )
			return True

		return False

	def update( self, detect, filter ):
		if( filter ):
			logging.debug( '[WHITELIST] %s' % self.whitelist() )
			logging.debug( '[BLACKLIST] %s' % self.blacklist() )
			self.updater.emit( detect.filter, self, detect )
