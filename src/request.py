#!/usr/bin/env python

from werkzeug.datastructures import MultiDict
from urlparse import urlparse

import logging

from itertools import chain as ichain, izip

from parameter import Parameter

class MultiTokenException( Exception ):
	def __init__( self ):
		Exception.__init__( self, "Only one token is supported. " \
				"Use the `token_hook` for further modifications." )

class Request( object ):
	TOKEN_SUB = '[@TOKEN]' # not for direct modification
	ONE_TOKEN = True
	def __init__( 	self,
					url = None,
					method = None,
					proto = None,
					host = None,
					port = None,
					path = None,
					get = None,
					post = None,
					files = None,
					headers = None,
					cookies = None,
					auth = None,
					proxies = None,
					timeout = None,
					session = None,
					token_type = None,
					token_key = None ):

		if( url is not None ):
			if( not(url.startswith('http://') or url.startswith('https://')) ):
				url = 'http://' + url
			url = urlparse( url )
			proto = url.scheme or 'http'
			host = url.hostname
			port = url.port
			path = filter( lambda x: x != '', url.path.split( '/' ) )
			key_val_pairs = lambda query: MultiDict([
					(y[0], (lambda z: z[1] if len(z) > 1 else '' )(y))
					for y in
					[ x.split('=',1) for x in query.split('&') ]
					if len(y) > 0 and y[0] != ''
			])
			get = key_val_pairs( url.query )
			method = 'GET'

		send_data = False

		if( get is not None ):
			get = MultiDict( get )

		if( post is not None ):
			send_data = True
			post = MultiDict( post )

		if( files is not None ):
			send_data = True
			files = MultiDict( files )

		if( send_data ):
			if( method is None or method.lower() == 'get' ):
				method = 'POST'

		self.host = host or None
		self.port = port or 80

		self.headers = headers or {}

		self.post = post or MultiDict()
		self.get = get or MultiDict()
		self.cookies = cookies or {}

		self.path = path or []

		self.method = method or None # GET
		self.proto = proto or None # HTTP

		self.auth = auth or () # requests format authentication 
		self.files = files or MultiDict() # requests format files
		self.proxies = proxies or {} # requests format proxies
		self.session = session or None # requests.Session
		self.timeout = timeout or 30 # requests timeout
		self.verify = False # requests verify SSL cert
		self.allow_redirects = True # requests allow_redirects for POST,PUT,DEL

		self.token_type = token_type # Parameter type
		self.token_key = token_key # xsrf token key

		isset_token = False

		map = lambda map,key: izip( map.iteritems(), [key] * len(map.keys()) )
		for ((key,val),type) in ichain( map( self.cookies, Parameter.COOKIE ),
										map( self.headers, Parameter.HEADER ),
										map( self.get, Parameter.GET ),
										map( self.post, Parameter.POST) ):
			# maps (key, val) tuples to their Parameter type
			if( val == self.TOKEN_SUB ):

				if( self.ONE_TOKEN and isset_token ):
					raise MultiTokenException()
				else:
					isset_token = True

				self.token_type = type
				self.token_key = key

class Token( Request ):
	def __init__( 	self,
					type,
					extract,
					**kwargs ):
		Request.__init__( self, **kwargs )

		self.extract_key = extract # token extraction string
		self.extract_type = type # token extraction method

class FuzzRequest( Request ):
	def __init__( 	self, 
					doauth = None, 
					reflections = None,
					chain = None,
					**kwargs ):

		Request.__init__( self, **kwargs )

		self.rtt = None # roundtrip for request
		self.epoch = None # epoch time of request start

		self.param = None # parameter key being fuzzed
		self.word = None # fuzz string for fuzzing
		self.type = None # parameter type being fuzzed
		self.fuzzer = None # fuzzer used to mutate request

		self.reflections = reflections or () # queries to fetch & analyse
		self.chain = chain or () # chain of requests to execute before this one

		self.body = None # original body
		self.null = None # body of non-existing key
		self.error = None # body of non-existing value
		self.not_found = None # body of 404
		self.index = None # body of index page
		self.rel_index = None # body of relative index page
		self.rel_not_found = None # body of relative not found page

		self.volatility = 0.0 # 0.0 - 1.0 simularity metric

		self.doauth = doauth # None -> ignore ; False -> logout ; True -> login
