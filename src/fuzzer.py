#!/usr/bin/env python

import sys

from pool import FuzzerPool
from requester import FuzzerRequester, Stealth
from auth import FuzzerAuth
from transformer import FuzzerTransformer, Transform, Obfuscation
from result import FuzzerResult
from anomoly import FuzzerAnomoly, Anomoly
from dispatcher import FuzzerDispatcher, Order, Fetch
from tech import FuzzerTech, Level, Tech

from query import FuzzQuery as Query
from query import FuzzRequest as Request
from query import TokenQuery as Token # rarely used
from query import Parameter, Extract # rarely used

from ifuzzer import FuzzGroup

from fuzzers.fuzz import Fuzz

from get import GETFuzzer
from post import POSTFuzzer
from cookie import CookieFuzzer
from path import PathFuzzer
from header import HeaderFuzzer
from generic import GenericFuzzer
from file import FileFuzzer

from patterns import Emitter, Action, Stream, Reactor, Adapter, Type, Event
from patterns import BreakStream, SilentBreakStream, Priority
from Queue import Queue, PriorityQueue, Empty
from functools import partial

import logging
import traceback
import time

from copy import deepcopy
from parameter import CRUD
from itertools import izip, chain as ichain

from fuzzers.custom import CustomFuzzer

class FuzzerFactory( object ):
	POOL = FuzzerPool
	RESULT = FuzzerResult
	AUTH = FuzzerAuth
	REQUESTER = FuzzerRequester
	TRANSFORMER = FuzzerTransformer
	ANOMOLY = FuzzerAnomoly
	DISPATCHER = FuzzerDispatcher
	TECH = FuzzerTech

	def __init__( self ):
		self.pool = None
		self.result = None
		self.auth = None
		self.requester = None
		self.transformer = None
		self.anomoly = None
		self.dispatcher = None
		self.tech = None

		self.mutable = False

	def create( self ):
		if( self.pool is None ):
			pool = FuzzerFactory.POOL()
		else:
			pool = self.pool

		if( self.result is None ):
			result = FuzzerFactory.RESULT()
		else:
			result = self.result

		if( self.auth is None ):
			auth = FuzzerFactory.AUTH()
		else:
			auth = self.auth

		if( self.requester is None ):
			requester = FuzzerFactory.REQUESTER()
		else:
			requester = self.requester

		if( self.transformer is None ):
			transformer = FuzzerFactory.TRANSFORMER()
		else:
			transformer = self.transformer

		if( self.anomoly is None ):
			anomoly = FuzzerFactory.ANOMOLY()
		else:
			anomoly = self.anomoly

		if( self.dispatcher is None ):
			dispatcher = FuzzerFactory.DISPATCHER()
		else:
			dispatcher = self.dispatcher

		if( self.tech is None ):
			tech = FuzzerFactory.TECH()
		else:
			tech = self.tech

		return Fuzzer( 	pool, 
						result,
						auth,
						requester,
						transformer,
						anomoly,
						dispatcher,
						tech,
						mutable = self.mutable )

class Fuzzer( object ):
	def __init__( 	self, 
					pool,
					result,
					auth,
					requester,
					transformer,
					anomoly,
					dispatcher, 
					tech,
					mutable = False ):

		self.pool = pool
		self.result = result
		self.auth = auth
		self.request = requester
		self.transform = transformer
		self.anomoly = anomoly
		self.dispatcher = dispatcher
		self.tech = tech

		self.mutable = mutable

	def register_fuzzer( self ):
		self.pool.register_fuzzer( self ) 
		self.result.register_fuzzer( self )
		self.auth.register_fuzzer( self )
		self.request.register_fuzzer( self )
		self.transform.register_fuzzer( self )
		self.anomoly.register_fuzzer( self )
		self.dispatcher.register_fuzzer( self )
		self.tech.register_fuzzer( self )
	
	def register_handlers( self, emitter ):
		self.pool.register_handlers( emitter )
		self.result.register_handlers( emitter )
		self.auth.register_handlers( emitter  )
		self.request.register_handlers( emitter )
		self.transform.register_handlers( emitter )
		self.anomoly.register_handlers( emitter )
		self.dispatcher.register_handlers( emitter )
		self.tech.register_handlers( emitter )

	def fuzz( self, map ):
		self.register_fuzzer()

		emitter = Emitter()

		self.register_handlers( emitter )

		for resource, fuzzers in map.items():
			fuzzers.register_fuzzer( self )
			if( not self.can_run( resource ) ):
				del map[ resource ]

		jobs = PriorityQueue()
		results = Queue()
		requests = Queue()
		work = Emitter()
		react = Emitter()

		self.request.register_queue( jobs )
		self.result.register_queue( results )

		prefetch = Queue()
		self.dispatcher.register_queues( jobs, prefetch )

		self.request.setup()
		self.auth.setup()

		self.dispatcher.fuzz( map )

		dispatcher = emitter.emit( Action.DISPATCHER )
		emitter.on( Action.DISPATCH, lambda: dispatcher.dispatch() )
		stream = Stream()

		def job_request():
			try:
				stream.pipe( emitter.emit, Action.DISPATCH ) \
						.pipe( emitter.emit, Action.TRANSFORM ) \
						.pipe( emitter.emit, Action.AUTH_INIT ) \
						.pipe( emitter.emit, Action.REQUEST_QUEUE ) \
						.reset()
			except StopIteration:
				return self.pool.stop()

		react.on( Type.JOB_REQUEST, job_request )

		reactor = Reactor( Adapter( requests, {'select': requests.get }) )
		reactor.register( react )

		def job( query ):
			stream = Stream( args=(query,) )
			try:
				stream.pipe( emitter.emit, Action.REQUEST_INIT ) \
						.pipe( emitter.emit, Action.AUTH_PRECHECK ) \
						.pipe( emitter.emit, Action.REQUEST_CHAIN ) \
						.pipe( emitter.emit, Action.REQUEST_XSRF ) \
						.pipe( emitter.emit, Action.REQUEST_SEND ) \
						.pipe( emitter.emit, Action.AUTH_POSTCHECK ) \
						.pipe( emitter.emit, Action.REQUEST_REFLECTIONS ) \
						.pipe( emitter.emit, Action.ANOMOLY ) \
						.pipe( emitter.emit, Action.TECH_DETECT ) \
						.pipe( emitter.emit, Action.RESULT )
			except BreakStream:
				stacktrace = traceback.format_exc()
				logging.debug( stacktrace )
				logging.error( '[BREAKSTREAM] @JOB %s' % query )
				self.result.error( query )
			except SilentBreakStream: # EXTREME CAUTION!!!
				logging.debug( '[SILENTBREAKSTREAM] %s' % query )
			finally:
				self.auth.free_session( query )
			requests.put(Event( Type.JOB_REQUEST ))

		work.on( Type.JOB, job )

		work.on(Action.STOP_REACTOR,
				lambda: Event( Action.STOP_REACTOR, Type.REACTOR ))

		def reflection( sync, query, reflection ):
			try:
				logging.debug( '[REFLECTION_RESOLUTION] %s for %s' % \
						( reflection, query ))
				self.request.send_stack( reflection )
				query.reflection_hook( reflection )
				if( not sync.release() ):
					return
				logging.debug( '[REFLECTION_AGGREGATION] %s' % query )
				for reflection in query.reflections():
					self.anomoly.reflection( query, reflection )
				stream = Stream( args=( query, ))
				stream.pipe( emitter.emit, Action.ANOMOLY ) \
						.pipe( emitter.emit, Action.TECH_DETECT ) \
						.pipe( emitter.emit, Action.RESULT )
			except BreakStream:
				logging.error( '[BREAKSTREAM] @REFLECTION %s for %s' % \
						( reflection, query ))
				sync.release()

		work.on( Type.REFLECTION, reflection )

		def prefetch( sync, result, query ):
			try:
				logging.debug( '[PREFETCH] {}'.format( query ))
				if( self.dispatcher.prefetch_reflection() ):
					self.request._prefetch_reflection( query )
				self.request.send_stack( query )
				result.put( query )
			except BreakStream:
				logging.error( '[BREAKSTREAM] @PREFETCH %s' % query )
			finally:
				sync.release()

		work.on( Type.PREFETCH, prefetch )

		worker = Reactor( Adapter( jobs, {'select': jobs.get }) )
		worker.register( work )

		for i in xrange( self.pool.size() ):
			requests.put(Event( Type.JOB_REQUEST ))

		self.pool.reactor( worker )
		self.pool.start()
		reactor.loop()
		return self

	def log( self, level = logging.DEBUG, filename = None ):
		logging.basicConfig(filename=filename, level=level,
							format='[%(asctime)s %(levelname)s] %(message)s',
							datefmt='%m/%d/%Y %I:%M:%S' )
		logging.getLogger().setLevel( level )
		requests_logger = logging.getLogger("requests")
		requests_logger.setLevel( logging.WARNING )
		return self

	def join( self ):
		self.pool.join()
		return self

	def send( 	self,
				query, 
				get = {},
				post = {},
				cookies = {},
				headers = {},
				path = {},
				files = {},
				bitmask = Fuzz.ALL,
				quiet = False ):

		if( not self.can_run( query ) ):
			return

		if( not self.mutable ):
			query = deepcopy( query )

		map = lambda map,key: izip( map.iteritems(), [key] * len(map.keys()) )
		for ((key,val),type) in ichain( map( cookies, Parameter.COOKIE ),
										map( headers, Parameter.HEADER ),
										map( get, Parameter.GET ),
										map( post, Parameter.POST),
										map( files, Parameter.FILE ),
										map( path, Parameter.PATH ) ):
			CRUD.setter.emit( type, query, key, val )

		self.register_fuzzer()
		emitter = Emitter()
		self.register_handlers( emitter )

		self.request.setup()
		self.auth.setup()

		custom_fuzzer = CustomFuzzer( bitmask = bitmask )
		custom_fuzzer.register_tech( self.tech )
		query.set_fuzzer( custom_fuzzer )

		self.result.register_queue( Queue() )

		stream = Stream( args=( query, )) \
						.pipe( emitter.emit, Action.TRANSMOGRIFY ) \
						.pipe( emitter.emit, Action.AUTH_INIT ) \
						.pipe( emitter.emit, Action.REQUEST_INIT ) \
						.pipe( emitter.emit, Action.AUTH_PRECHECK ) \
						.pipe( emitter.emit, Action.REQUEST_CHAIN ) \
						.pipe( emitter.emit, Action.REQUEST_XSRF ) \
						.pipe( emitter.emit, Action.REQUEST_SEND )

		if( self.dispatcher.doprefetch() or len( query.reflections() ) > 0 ):
			# ! no point in prefetch for reflections at this point

			queue = Queue()
			prefetch = Queue()
			worker = Emitter()

			self.dispatcher.register_queues( queue, prefetch )

			def reflection( query, reflection ):
				logging.debug( 	'[REFLECTION_RESOLUTION] ' \
								'%s for %s' % ( query, reflection ) )
				self.request.send_stack( reflection )
				query.reflection_hook( reflection )
				self.anomoly.reflection( query, reflection )
			worker.on( Type.REFLECTION, reflection )

			worker.on( Action.STOP_REACTOR,
						lambda: Event( Action.STOP_REACTOR, Type.REACTOR ))

			reactor = Reactor( Adapter( queue, {'select': queue.get} ))
			reactor.register( worker )
			self.pool.reactor( reactor )
			for reflection in query.reflections():
				queue.put( Event( Type.REFLECTION, args=( query, reflection )))
			self.pool.start()
			queue.join()

			for i in range( self.pool.size() ):
				self.pool.stop()

		stream.pipe( emitter.emit, Action.AUTH_POSTCHECK ) \
				.pipe( emitter.emit, Action.ANOMOLY ) \
				.pipe( emitter.emit, Action.TECH_DETECT ) \
				.pipe( emitter.emit, Action.RESULT )

		if( not quiet ):
			for anomoly in query.anomolies():
				print anomoly

		query.freeze()

		return query

	def can_run( self, query ):
		if( not query.ismutable() ):
			logging.error( '{} is read only'.format( query ) )
			return False
		return True
