#!/usr/bin/env python

from nose import with_setup
from nose.tools import assert_equals

import path

from fuzzer import *
from Queue import Queue, Empty

class BaseFuzzer( object ):
	def _get_results( self ):

		results = []
		while True:
			try:
				results.append( self._results.get_nowait() )
			except Empty:
				break
		return results

class TestFuzzer( BaseFuzzer ):
	def setup( self ):
		self._results = Queue()
		this = self

		def normal( self, query ):
			logging.info( '[*] OK %s' % query )

		def anomoly( self, query ):
			logging.warning( '[!] ANOMOLY %s' % query )
			this._results.put( query )

		def error( self, query ):
			logging.error( '[-] ERROR %s' % query )

		self.factory = FuzzerFactory()
		self.factory.transformer = FuzzerTransformer( Transform.GOOGLE_CHROME )
		self.factory.result = FuzzerResult( anomoly = anomoly, 
											normal = normal,
											error = error )
		self.factory.dispatcher = FuzzerDispatcher( fetch = Fetch.ALL )
		self.factory.pool = FuzzerPool( 3 )
		self.factory.anomoly = FuzzerAnomoly( Anomoly.ALL | Anomoly.SMART | Anomoly.FILTER )

		self.fuzzer = self.factory.create()
		self.fuzzer.log( level = logging.INFO )

		self.fuzzer.auth = FuzzerAuth( 	login = Query( 	url = 'http://127.0.0.1:5050/login?user=user&pass=pass&token=[@TOKEN]',
														token = Token( 	url = 'http://127.0.0.1:5050/token',
																		type='header',
																		extract='X-XSRF' )),
										logout = Query( url = 'http://127.0.0.1:5050/logout?token=[@TOKEN]',
														token = Token( 	url = 'http://127.0.0.1:5050/token',
																		type = 'header',
																		extract = 'X-XSRF' )),
										context = True )

	def test_secure( self ):

		q = Query( url = 'http://127.0.0.1:5050/secure?one=1&two=2&three=3&four=4' )
		self.fuzzer.fuzz({ q: GETFuzzer( Fuzz.DEMO ) })
		self.fuzzer.join()

		assert( len(self._get_results()) == 0 )

	def test_vulnerable( self ):
		q = Query( url = 'http://127.0.0.1:5050/vulnerable?vuln=0&one=1&two=2&three=3&four=4' )
		self.fuzzer.fuzz({ q: GETFuzzer( Fuzz.DEMO ) })
		self.fuzzer.join()

		assert( len(self._get_results()) == 1 )

	def test_fuzzgroup( self ):
		q = Query( url = 'http://127.0.0.1:5050/vulnerable?vuln=0&one=1&two=2&three=3&four=4' )
		self.fuzzer.fuzz({ q: FuzzGroup( GETFuzzer( Fuzz.DEMO ), GETFuzzer( Fuzz.DEMO ) ) })
		self.fuzzer.join()

		assert( len(self._get_results()) == 2 )

	def test_multi_query( self ):
		q1 = Query( url = 'http://127.0.0.1:5050/vulnerable?vuln=0&one=1&two=2&three=3&four=4' )
		q2 = Query( url = 'http://127.0.0.1:5050/vulnerable?vuln=0&one=1&two=2&three=3&four=4' )
		self.fuzzer.fuzz({
			q1: GETFuzzer( Fuzz.DEMO ),
			q2: GETFuzzer( Fuzz.DEMO ),
		})
		self.fuzzer.join()

		assert( len(self._get_results()) == 2 )

	def test_scope_include( self ):
		q = Query( url = 'http://127.0.0.1:5050/vulnerable?vuln=0&one=1&two=2&three=3&four=4' )
		self.fuzzer.fuzz({ q: GETFuzzer( Fuzz.DEMO, scope = ( 'vuln', )) })
		self.fuzzer.join()

		assert( len(self._get_results()) == 1 )

	def test_scope_exclude( self ):
		q = Query( url = 'http://127.0.0.1:5050/vulnerable?vuln=0&one=1&two=2&three=3&four=4' )
		self.fuzzer.fuzz({ q: GETFuzzer( Fuzz.DEMO, exclude = ( 'vuln', )) })
		self.fuzzer.join()

		assert( len(self._get_results()) == 0 )

	def test_token( self ):
		q = Query( 	url = 'http://127.0.0.1:5050/vulnerable_with_token?vuln=0&one=1&two=2&three=3&four=4&token=[@TOKEN]',
					token = Token( 	url = 'http://127.0.0.1:5050/token',
									type = 'header',
									extract = 'X-XSRF' ))
		self.fuzzer.fuzz({ q: GETFuzzer( Fuzz.DEMO ) })
		self.fuzzer.join()

		assert( len(self._get_results()) == 1 )

	def test_auth( self ):
		q = Query( 	url = 'http://127.0.0.1:5050/vulnerable_with_auth?vuln=0&one=1&two=2&three=3&four=4',
					doauth = True )
		self.fuzzer.fuzz({ q: GETFuzzer( Fuzz.DEMO ) })
		self.fuzzer.join()

		assert( len(self._get_results()) == 1 )
		
	def test_auth_token( self ):
		q = Query( 	url = 'http://127.0.0.1:5050/vulnerable_with_auth_with_token?vuln=0&one=1&two=2&three=3&four=4&token=[@TOKEN]',
					token = Token( 	url = 'http://127.0.0.1:5050/token',
									type = 'header',
									extract = 'X-XSRF' ),
					doauth = True )
		self.fuzzer.fuzz({ q: GETFuzzer( Fuzz.DEMO ) })
		self.fuzzer.join()

		assert( len(self._get_results()) == 1 )

	def test_deauth( self ):
		qa = Query( url = 'http://127.0.0.1:5050/vulnerable_with_auth_with_token?zero=0&one=1&two=2&token=[@TOKEN]',
					token = Token( 	url = 'http://127.0.0.1:5050/token',
									type = 'header',
									extract = 'X-XSRF' ),
					doauth = True )

		qd = Query( url = 'http://127.0.0.1:5050/vulnerable_without_auth?vuln=0&one=1&two=2',
					doauth = False )

		self.fuzzer.fuzz({ qa: GETFuzzer( Fuzz.DEMO ), qd: GETFuzzer( Fuzz.DEMO ) })
		self.fuzzer.join()

		assert( len(self._get_results()) == 1 )

	def test_reauth( self ):
		qa = Query( url = 'http://127.0.0.1:5050/vulnerable_with_auth_with_token?zero=0&one=1&two=2&token=[@TOKEN]',
					token = Token( 	url = 'http://127.0.0.1:5050/token',
									type = 'header',
									extract = 'X-XSRF' ),
					doauth = True )

		qd = Query( url = 'http://127.0.0.1:5050/vulnerable_without_auth?zero=0&one=1&two=2',
					doauth = False )

		qr = Query( url = 'http://127.0.0.1:5050/vulnerable_with_auth_with_token?vuln=0&one=1&two=2&token=[@TOKEN]',
					token = Token( 	url = 'http://127.0.0.1:5050/token',
									type = 'header',
									extract = 'X-XSRF' ),
					doauth = True )

		self.fuzzer.fuzz(dict(zip([qa,qd,qr],[GETFuzzer(Fuzz.DEMO)]*3 )))
		self.fuzzer.join()

		assert( len(self._get_results()) == 1 )

	def test_reflections( self ):
		q = Query( 	url = 'http://127.0.0.1:5050/secure?one=1&two=2&three=3&four=4',
					reflections = [
						Query( url = 'http://127.0.0.1:5050/vulnerable?vuln=fuzz' ),
						Query( url = 'http://127.0.0.1:5050/vulnerable?vuln=fuzz' ),
						Query( url = 'http://127.0.0.1:5050/vulnerable?vuln=fuzz' ),
					])
		self.fuzzer.fuzz({ q: GETFuzzer( Fuzz.DEMO ) })
		self.fuzzer.join()

		assert( len(self._get_results()) == 1 )
