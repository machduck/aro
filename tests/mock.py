#!/usr/bin/env python

from flask import Flask
from flask import request
from flask import session
from flask import render_template_string, Response

app = Flask( __name__ )
app.secret_key = "mock"

from xsrf import XSRFHandler
xsrfh = XSRFHandler( app )

from simplelogin import Login
login = Login()

def vuln():
	vuln = request.args.get( 'vuln', '' )
	if vuln == 'fuzz':
		return 'detect'
	return 'secure'

@app.route( '/secure' )
def secure():
	return 'secure'

@app.route( '/vulnerable' )
def vulnerable():
	return vuln()

@app.route( '/vulnerable_with_token' )
@xsrfh.verify()
def vulnerable_with_token():
	return vuln()
	
@app.route( '/token' )
@xsrfh.send()
def token():
	return Response( render_template_string( '{{ xsrf_token }}' ))

@app.route( '/login' )
@xsrfh.verify()
@login.login()
def _login():
	return Response( 'success' )

@app.route( '/logout' )
@xsrfh.verify()
@login.logout()
def _logout():
	return Response( 'success' )

@app.route( '/vulnerable_with_auth' )
@login.required()
def vulnerable_with_auth():
	return vuln()

@app.route( '/vulnerable_with_auth_with_token' )
@xsrfh.verify()
@login.required()
def vulnerable_with_auth_with_token():
	return vuln()

@app.route( '/vulnerable_without_auth' )
def vulnerable_without_auth():
	if( Login.LOGIN_COOKIE not in session 
			or session[ Login.LOGIN_COOKIE ] != Login.LOGIN_OK ):
		return vuln()
	return 'secure'

if __name__ == "__main__":
	app.run( host = '127.0.0.1', debug = True, port = 5050 )
